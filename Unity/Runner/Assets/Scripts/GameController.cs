﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public PlayerController player;
    public Camera camara;
    public GameObject[] blockPrefabs;
    private int contador = 0;

    private float puntero;
    //private float spawn = 30;

    // Start is called before the first frame update
    void Start()
    {
        //Creo el blockInit
        Instantiate(blockPrefabs[0]);

    }

    // Update is called once per frame
    void Update()
    {
        //Depende de en la escena en la que esta hace una cosa o otra
        if (!PlayerController.cambioEscena)
        {
            //La camara sigue al personaje y en las x le pongo +7 para que el personaje se quede a un lado de la pantalla
            camara.transform.position = new Vector3(player.transform.position.x + 7f, camara.transform.position.y, camara.transform.position.z);

            if (player != null && this.puntero < player.transform.position.x)
            {
                GameObject newBlock;
                if (contador == 10)
                {
                    //El block que creo cuando llega el contador a 10 es el ultimo en la array que es el que hace que cambies de escena y pongo el contador a 0
                    newBlock = Instantiate(blockPrefabs[blockPrefabs.Length - 1]);
                    contador = 0;
                }
                else
                {
                    //instanciar hace aparecer en la escena activa
                    newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length - 1)]);
                }
                //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
                float size = newBlock.transform.GetChild(0).transform.localScale.x;
                //ponemos la posicion del tamaño instanciado en la posicion del puntero + su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
                newBlock.transform.position = new Vector2(puntero + size, 0);
                //aumentamos el puntero en el tamaño del bloque
                puntero += size;
                contador++;
            }
        }else if (PlayerController.cambioEscena)
        {
            //La camara sigue al personaje y en las x le pongo -7 para que el personaje se quede a un lado de la pantalla
            camara.transform.position = new Vector3(player.transform.position.x - 7f, camara.transform.position.y, camara.transform.position.z);
            
            if (player != null && this.puntero > player.transform.position.x)
            {
                GameObject newBlock;
                if (contador == 10)
                {
                    //El block que creo cuando llega el contador a 10 es el ultimo en la array que es el que hace que cambies de escena y pongo el contador a 0
                    newBlock = Instantiate(blockPrefabs[blockPrefabs.Length - 1]);
                    contador = 0;
                }
                else
                {
                    //instanciar hace aparecer en la escena activa
                    newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length - 1)]);
                }
                    //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
                    float size = newBlock.transform.GetChild(0).transform.localScale.x;
                    //ponemos la posicion del tamaño instanciado en la posicion del puntero - su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
                    newBlock.transform.position = new Vector2(puntero - size, 0);
                    //disminuimos el puntero en el tamaño del bloque
                    puntero -= size;
                    contador++;
                }
                
            }
        }
        
    }












