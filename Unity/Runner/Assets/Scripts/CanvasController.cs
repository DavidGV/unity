﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    //Range es para crear una barra en el menu de los componentes de los objetos, te aseguras de que el valor de la variable este entre los valores que le pones
    //parallaxSpeed velocidad a la que se mueve el fondo
    [Range (0f, 0.20f)]
    public float parallaxSpeed = 0.02f;
    public PlayerController player;
    public Canvas canvas;
    public RawImage background2;
    public RawImage background3;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Paras que el fondo sigua al personeje creamos un nuevo vector3, en las x le pongo +7 para que el personaje se quede a un lado de la pantalla
        canvas.transform.position = new Vector3(player.transform.position.x + 7f, canvas.transform.position.y, canvas.transform.position.z);
        //En finalSpeed multiplico el parralaxSpeed por el deltaTime para que vaya a corde con el tiempo del juego
        float finalSpeed = parallaxSpeed * Time.deltaTime;
        //Creo un nuevo Rect con la nueva posicion en las x, al irse actualizando con el Update() da el efecto de movimiento
        background2.uvRect = new Rect(background2.uvRect.x + finalSpeed, 0f, 1f, 1f);
        background3.uvRect = new Rect(background3.uvRect.x + finalSpeed * 4, 0f, 1f, 1f);
    }
}
