﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Puntuacion : MonoBehaviour
{
    // Start is called before the first frame update
    public static int score;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //El texto sera "Score:" + el valor de la variable score
        this.GetComponent<Text>().text = "Score:"+score;
        //Metemos una variable con el nombre "Score" dentro del PlayerPrefs con el valor de score(Es como un map (clave-valor)) 
        PlayerPrefs.SetInt("Score", score);
    }
}
