﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escoba : MonoBehaviour
{   
    public PlayerController player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Dependiendo en que escena este objeto estara a una distancia alejada del personaje y siempre por detras
        if (!PlayerController.cambioEscena)
        {
            this.transform.position = new Vector3(player.transform.position.x - 50f, this.transform.position.y, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(player.transform.position.x + 50f, this.transform.position.y, this.transform.position.z);
        }
        
    }

    //Este objeto borra todo con lo que colisione
    void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
    }


}
