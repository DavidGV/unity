﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ChangeSceneInit : MonoBehaviour
{
    private bool activar = false;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("ActivarBool", 1);
    }

    // Update is called once per frame

    void Update()
    {
        //Si pulsas Return(intro del teclado) activas el bool PlayerController.gameOver para resetear las variables iniciales y cargas la pantalla de juego
        //if (Input.GetKeyDown(KeyCode.Return))
    }
    public void Continue()
    {
        if (activar)
        {
            SceneManager.LoadScene("Game");
        }
        
    }
    private void ActivarBool()
    {
        activar = true;
    }
}
