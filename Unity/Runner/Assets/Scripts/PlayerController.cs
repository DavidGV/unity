﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //bool para cuando mueres
    public static bool gameOver = false;
    public float jumpForce = 12f;
    public int score = 0;
    //bool para cambia ente las escenas de juego
    public static bool cambioEscena = false;
    //Sonido
    public AudioClip calabazaSound;
    private AudioSource calabazaSoundSource;
    private Animator anim;
    private bool canJump = true;
    public static bool agachado = false;
    //Contador de cuantos obstaculos le queda para que el juego vuleva a ser normal despues de haberte comido el powerup de la calabaza
    private static int calabazaCont = 0;
    //Cuando te has comido el powerup de la calabaza
    public static bool calabaza = false;
    private static float speed = 500f;


    //Awake para que se genere antes del Start()
    public void Awake()
    {
        //En el caso de haber perdido reinicar las variables a su valor por defecto
        if (gameOver)
        {
            Puntuacion.score = 0;
            gameOver = false;
            speed = 500f;
            calabaza = false;
            calabazaCont = 0;

        }
        //En el caso de que cambias de escena de juego y tienes el power up de calabaza activo lo mantienes en la siguiente escena
        if (calabazaCont>0)
        {
            //Powerup calabaza
            //Gravedad a 0 para que no se caiga
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            //Es triger para que no colisione con obstaculos
            this.GetComponent<CapsuleCollider2D>().isTrigger = true;
            //Le cambio el color para que sea visual el efecto del powerup
            this.GetComponent<SpriteRenderer>().color = new Color(255 / 255f, 144 / 255f, 0 / 255f); 
            //Le cambio la posicion para que vaya por encima del suelo
            this.transform.position = new Vector3(this.transform.position.x, -2.6f, this.transform.position.z);
            //Le reseteo la velocidad en y
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed * Time.deltaTime, 0);
            //Activo el bool del powerup calabaza
            calabaza = true;
        }
        //Inicializo el sonido
        calabazaSoundSource = GetComponent<AudioSource>();


    }
    public void Start()
    {

    }

    public void Update()
    {
        //Accedo al componente animato y le digo que el bool que tiene apodado "Agachado" en mi codigo es el bool agachado
        this.GetComponent<Animator>().SetBool("Agachado", agachado);
        //Si estamos en el powerup de calabaza y el contador es 0 volvemos al modo normal de juego quitando el modo trigger, poniendo la gravedad por defecto, poniendo su color
        //y desactivando el powerup calabaza
        if (calabazaCont == 0 && calabaza)
        {

            this.GetComponent<Rigidbody2D>().gravityScale = 2;
            this.GetComponent<CapsuleCollider2D>().isTrigger = false;
            this.GetComponent<SpriteRenderer>().color = Color.white;
            calabaza = false;
            //Debug.Log("END");
        }
        //Si tenemos el powerup calabaza no nos podemos mover
        if (!calabaza)
        {
            ProcessInput();
        }
        MoreVelocity();
    } 

    //En el FixedUpdate he puesto la velocidad para que en cualquier dispositivo en el que se juege vaya a la misma velocidad
    public void FixedUpdate()
    {
         this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed * Time.deltaTime, this.GetComponent<Rigidbody2D>().velocity.y);
    }
    //ProcessInput() son los dos movimientos que se pueden hacer en este juego
    private void ProcessInput()
    {
        //Tecla "w" para saltar y Tecla "s" para agacharse
        /*if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touch_Pos = Camera.main.ScreenToWorldPoint(touch.position);
            if (touch_Pos.x > 0)
            {
                Jump();
            }
            else if (touch_Pos.x < 0)
            {
                agachado = true;
            }
        }
        else if (Input.touchCount <= 0 && agachado)
        {
            agachado = false;
        }*/
        /*if (Input.GetKeyDown("w"))
        {
            Jump();
        }
        else if (Input.GetKey("s"))
        {
            agachado = true;

        } else if (!Input.GetKey("s") && agachado)
        {
            agachado = false;
        }*/
    }

    //Jump() para saltar
    private void Jump()
    {
        //En el caso de que estemos en el suelo podremos saltar
        if (canJump && !calabaza)
        {
            canJump = false;
            //Añado fuerza de salto, con la funcio que tengo comentada abajo, dependiendo del momento en el que pulsaba la tecla de salto saltaba mas o menos alto,
            //busque por internet y me encontre con el ForceMode2D.Impulse que ya tiene creado unity para hacer saltos en juegos 2D y me salta siempre igual
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            //this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce * Time.deltaTime);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si colisionas con el suelo puedes saltar
        if (collision.gameObject.tag == "Ground")
        {
            canJump = true;
        }
        //Si colisionas con el obstaculo pierdes
        if (collision.gameObject.tag == "Obstaculo")
        {
           Perder();
        }
        
    }
    //Cada 20 posiciones en el eje x la velocidad amuenta hasta llegar a 1000
    void MoreVelocity()
    {
        if (!cambioEscena)
        {
            if (Mathf.Floor(this.transform.position.x) % 20 == 0 && speed < 1000f)
            {
                speed += 2f;
                // Debug.Log("Vel:" + speed);
            }
        } else if (cambioEscena)
        {
            if (Mathf.Floor(this.transform.position.x) % 20 == 0 && speed > -1000f)
            {
                speed -= 2f;
                // Debug.Log("Vel:" + speed);
            }
        }
    }
   
    //
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si colisionas con el triger de la calabaza
        if (collision.gameObject.tag == "Calabaza")
        {
            //Powerup calabaza
            //Destruyes la calabaza
            Destroy(collision.gameObject);
            //Gravedad a 0 para que no se caiga
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            //Es triger para que no colisione con obstaculos
            this.GetComponent<CapsuleCollider2D>().isTrigger = true;
            //Le cambio el color para que sea visual el efecto del powerup
            this.GetComponent<SpriteRenderer>().color = new Color(255 / 255f, 144 / 255f, 0 / 255f);
            //Le cambio la posicion para que vaya por encima del suelo
            this.transform.position = new Vector3(this.transform.position.x, -2.6f, this.transform.position.z);
            //Le reseteo la velocidad en y
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed * Time.deltaTime, 0);
            //El contador se pone a 5
            calabazaCont = 5;
            //Activo el bool del powerup calabaza
            calabaza = true;
            //Suena el sondio una vez
            calabazaSoundSource.PlayOneShot(calabazaSound);
        }
        //Si colisionas con un trigger "Score" te suma puntuacion y en el caso de que estes con el powerup activo te da mas puntos y le resta una al contador de calabazas,
        //el contador de calabazas lo resto aqui para que siempre me asegure de que el personaje pierda el powerup justo despues de un obstaculo y tenga tiempo para reaccionar
        if (collision.gameObject.tag == "Score")
        {
            if (calabaza)
            {
                Puntuacion.score += 150;
                calabazaCont--;
             
            }
            else
            {
                Puntuacion.score += 50;
            }
           
        }
        //Si colisionas con el trigger de "Cambio" cambio entre las dos escenas de juego que hay, activo o desactivo el bool y cambio el signo de la velocidad
        if (collision.gameObject.tag == "Cambio")
        {
            if (!cambioEscena)
            {
                cambioEscena = true;
                speed = -speed;
                SceneManager.LoadScene("Game2");
            }
            else if (cambioEscena)
            {
                cambioEscena = false;
                speed = -speed;
                SceneManager.LoadScene("Game");
            }
            
        }
    }
    //La funcion Perder() pone el bool de cambio de escena en false para que siempre empieze el juego en la primera escena con todos los valores correctos y te envia 
    //a la escena "Dead"
    void Perder()
    {
        cambioEscena = false;
        SceneManager.LoadScene("Dead");
    }
    public void Saltar()
    {
        Jump();
    }



}
