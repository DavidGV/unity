﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeImg : MonoBehaviour
{
    public GameObject img2;
    private bool show = false;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CambiarFondo", 0.5f, 0.5f);
    }

   
    void CambiarFondo() {
        if (show)
        {
            show = false;
            img2.SetActive(false);

        }else
        {
            show = true;
            img2.SetActive(true);
        }
        
    }


}
