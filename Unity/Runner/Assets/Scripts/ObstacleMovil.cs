﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovil : MonoBehaviour
{
    //Ayuda de internet
    //target es a donde quiere ir el obstaculo
    public Transform target;
    public float speed;

    private Vector3 start, end;

    // Start is called before the first frame update
    void Start()
    {
        //Si el target no es null le digo que sea null y le digo que la variable start es donde empieza el obstaculo y la variable end sea el target(donde tiene que ir)
        if (target != null)
        {
            target.parent = null;
            start = transform.position;
            end = target.position;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        //Si el target no es null el obstaculo se mueve des de donde esta hasta al target y con la velocidad multiplicada por el deltaTime(fixedSpeed)
        if (target != null)
        {
            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, fixedSpeed);
        }
        //Si el obstaculo ha llegado a la posicion del target con una ternaria le digo que la posicion del target es igual a la del principio le assigno a la posicion
        //del end al target, en el caso contrario la posicion del target sera la del principio
        if (transform.position == target.position)
        {
            target.position = (target.position == start) ? end : start;
        }


    }
}
