﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ComeBack : MonoBehaviour
{
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        //Si pulsas Return(intro del teclado) activas el bool PlayerController.gameOver para resetear las variables iniciales y cargas la pantalla de juego
        //if (Input.GetKeyDown(KeyCode.Return))
        if (Input.touchCount > 0)
        {
            PlayerController.gameOver = true;
            SceneManager.LoadScene("Initial");
        }
        //Le digo que el texto es la variable "Score" almazenda en PlayerPrefs
        this.GetComponent<Text>().text = ""+ PlayerPrefs.GetInt("Score");
    }
}
