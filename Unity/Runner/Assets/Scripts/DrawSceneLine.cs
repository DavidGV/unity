﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSceneLine : MonoBehaviour
{
    public Transform from;
    public Transform to;

    //Accedeo al Gizmos y creo una linea del obstaculo hasta el target con el color y ancho que quiero (Solo para que sea mas visual)(Ayuda en internet)
    void OnDrawGizmosSelected()
    {
        if (from != null && to != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(from.position, to.position);
            Gizmos.DrawSphere(from.position, 0.15f);
            Gizmos.DrawSphere(to.position, 0.15f);
        }
    }
}
