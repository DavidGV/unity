﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BtnAgacharte : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!PlayerController.calabaza)
        {
            PlayerController.agachado = true;
            Debug.Log("OnPointerDown");
        }
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (PlayerController.agachado)
        {
            PlayerController.agachado = false;
            Debug.Log("OnPointerUp");
        }
        
    }
}