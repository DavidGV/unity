﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubZeroController : MonoBehaviour
{   //Bools para movimientos y ataques
    private bool adelante = false;
    private bool atras = false;
    public bool salto = false;
    private bool agachado = false;
    private bool h = false;
    private bool m = false;
    private bool l = false;
    private bool canJump = true;
    //Floats de velocidades
    public float velAdelante = 4f;
    public float velAtras = -2.5f;
    public float velAdelanteTemp = 4f;
    public float velAtrasTemp = -2.5f;
    //Floats de fuerza para el salto
    public float jumpForce = 15f;
    //Ints de vida, ataque y cooldown
    public int vida = 300;
    public int ataque = 20;
    private int cooldown = 0;
    //GameObject Scorpion
    public GameObject scorpionMuneco;
    //Bools de animaciones
    public bool dolor = false;
    public bool bloquear = false;
    public bool muerto = false;
    public bool mareos = false;
    public bool suelo = false;
    public bool noMover = false;
    public bool levantar = false;
    public bool fatality = false;
    private bool pared = false;
    //Delegado para que la barra de vida se actualize
    public delegate void PerderVidaSubzero();
    public event PerderVidaSubzero eventPerderVidaSubzero;
    //Delegado para poner el texto de finish
    public delegate void finishSubzero();
    public event finishSubzero eventfinishSubzero;
    //Delegado para quitar el texto de finish
    public delegate void finishStopSubzero();
    public event finishStopSubzero eventfinishStopSubzero;
    //GameObjects de proyectiles
    public GameObject hielo;
    public GameObject bolaNieve;
    public GameObject hieloCombo;
    public GameObject hieloFatality;
    //GameObject de particulas de sangre
    public GameObject sangre;
    //Status y breack de las corrutinas 
    private bool cBreak;
    public int status;
    //Sonido
    public AudioClip hit;
    public AudioClip scorpionWins;
    public AudioClip fatalityGrito;
    public AudioSource hitSource;

    void Start()
    {
        //buscar Scorpion
        scorpionMuneco = GameObject.Find("Scorpion");
        //Suscripciones de los delegados de tus hijos para recibir daño, impulsarte con el golpe
        this.GetComponentInChildren<RecibirDanoSubzero>().eventPerderVidaSubzeroChild1 += RecibirDano;
        this.GetComponentInChildren<RecibirDanoSubzero2>().eventPerderVidaSubzeroChild2 += RecibirDano;
        this.GetComponentInChildren<RecibirDanoSubzero>().eventPerderVidaSubzeroChild1 += ImpulsoHit;
        this.GetComponentInChildren<RecibirDanoSubzero2>().eventPerderVidaSubzeroChild2 += ImpulsoHit;
        //cambio de lado
        this.transform.Rotate(0, 180, 0);
        //Suscripcion al delegado de cambio de lado
        scorpionMuneco.gameObject.GetComponent<ScorpionController>().eventcambioDeLado += CambiarElLado;
        //Sonido
        hitSource = this.GetComponent<AudioSource>();

    }
    void Update()
    {
        //Bools de animaciones
        this.GetComponent<Animator>().SetBool("Adelante", adelante);
        this.GetComponent<Animator>().SetBool("Atras", atras);
        this.GetComponent<Animator>().SetBool("Salto", salto);
        this.GetComponent<Animator>().SetBool("Agachado", agachado);
        this.GetComponent<Animator>().SetBool("H", h);
        this.GetComponent<Animator>().SetBool("M", m);
        this.GetComponent<Animator>().SetBool("L", l);
        this.GetComponent<Animator>().SetBool("Dolor", dolor);
        this.GetComponent<Animator>().SetBool("Bloquear", bloquear);
        this.GetComponent<Animator>().SetBool("Muerto", muerto);
        this.GetComponent<Animator>().SetBool("Levantar", levantar);
        this.GetComponent<Animator>().SetBool("Fatality", fatality);
        //Mover a la derecha
        if (Input.GetKey("right") && !noMover )
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velAtras, this.GetComponent<Rigidbody2D>().velocity.y);
            if (!scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
            {
                adelante = false;
                atras = true;
            }
            else
            {
                adelante = true;
                atras = false;
            }

        }//Mover a la izquierda
        else if (Input.GetKey("left") && !noMover )
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velAdelante, this.GetComponent<Rigidbody2D>().velocity.y);
            if (!scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
            {
                adelante = true;
                atras = false;
            }
            else
            {
                adelante = false;
                atras = true;
            }
        }//Quedarte quieto
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            adelante = false;
            atras = false;
        }//Saltar
        if (Input.GetKeyDown("up") && canJump && !noMover)
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            canJump = false;
            salto = true;
            agachado = false;

        }//Agacharte, al agacharte no te puedes mover horizontalmente
        else if (Input.GetKey("down") && !noMover)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            adelante = false;
            atras = false;
            agachado = true;
        }//Ponerte de pie
        else
        {
            agachado = false;
        }//Golpe L, tambien inicia combo, en el caso de que el enemigo este con el bool activo de mareos empieza otro combo
        if (Input.GetKeyDown("j") && !noMover)
        {
            l = true;
            cBreak = false;
            if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().mareos)
            {
                StartCoroutine(MinCombo(0f, 1));
                StartCoroutine(MaxCombo(1f, status));
            }
            else
            {
                StartCoroutine(MinCombo(0.25f, 1));
                StartCoroutine(MaxCombo(1.5f, status));
            }
        }//Quita el golpe L
        else
        {
            l = false;
        }//Golpe M, si has emepezado combo lo continuas, en el caso de que el enemigo este con el bool activo de mareos activas AtaqueFatality()
        if (Input.GetKeyDown("k") && !noMover)
        {
            if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().mareos)
            {
                if (status == 2)
                {
                    AtaqueFatality();
                }
                else
                {
                    m = true;
                }
            }
            else
            {
                if (status == 1)
                {
                    m = true;
                    StartCoroutine(MinCombo(0.25f, 2));
                    StartCoroutine(MaxCombo(1.5f, status));
                }
                else
                {
                    m = true;
                }
            }

        }//Quita el golpe M
        else
        {
            m = false;
        }//Golpe H, si estaba el combo activos lo acabas llamando a Combo(), en el caso de que el enemigo este con el bool activo de mareos puedes continuar el combo
        if (Input.GetKeyDown("l") && !noMover)
        {
            if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().mareos)
            {
                if (status == 1)
                {
                    StartCoroutine(MinCombo(0f, 2));
                    StartCoroutine(MaxCombo(1f, status));
                }
                else
                {
                    h = true;
                    cBreak = true;
                }
            }
            else
            {
                if (status == 2)
                {
                    Combo();
                    status = 0;
                }
                else
                {
                    h = true;
                    cBreak = true;
                }
            }
        }//Quita el golpe H
        else
        {
            h = false;
        }//Bloqueas golpes pero no puedes moverte horizontalmente
        if (Input.GetKey("i") && !noMover)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            bloquear = true;
        }//Dejas de bloquear
        else
        {
            bloquear = false;
        }//Lanzas un proyectil, si solo pulsas "g" el disparo va recto y congela, si ademas vas hacia delante es un tiro aerio con parabola
        if (Input.GetKeyDown("o") && !noMover)
        {   //Si puedes disparar
            if (cooldown == 50)
            {
                if (adelante)
                {
                    DisparoAerio();
                }
                else
                {
                    Disparo();
                }
                cooldown = 0;
            }
        }
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {   //Si colisionas con el suelo puedes volver a saltar
        if (collision.gameObject.tag == "Suelo")
        {
            canJump = true;
            salto = false;
            pared = false;
        }//Si colisionas con la pared y estas saltando activas el bool
        if (collision.gameObject.tag == "Pared" && salto)
        {
            Debug.Log("Hola");
            pared = true;
            this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }

    }
    private void FixedUpdate()
    {   //Actualizar el cooldown de disparo
        if (cooldown < 50)
        {
            cooldown++;
        }
    }
    void LateUpdate()
    {
        //Si estas estas en la pared mientras saltas bajas 
        if (pared)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }//Si el delegado ha sido activado cambias de lado y tambien las velocidades
    void CambiarElLado()
    {
        this.transform.Rotate(0, 180, 0);
        hielo.transform.Rotate(0, 180, 0);
        if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
        {
            velAdelante = -velAtrasTemp;
            velAtras = -velAdelanteTemp;
        }
        else
        {
            velAdelante = velAdelanteTemp;
            velAtras = velAtrasTemp;
        }
    }//Instancia un disparo horizontal en la direcciona a la que mira el personaje que congela al enemigo
    void Disparo()
    {
        if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
        {
            print("hola " + this.transform.position);
            GameObject projectil = Instantiate(hielo);
            projectil.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y);
            projectil.GetComponent<Hielo>().vel = 7f;
        }
        else
        {
            print("borrego " + this.transform.position);
            GameObject projectil = Instantiate(hielo);
            projectil.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y);
            projectil.GetComponent<Hielo>().vel = -7f;
        }
    }//Instancia un disparo aerio con parabola en la direcciona a la que mira el personaje
    void DisparoAerio()
    {
        if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
        {
            GameObject bola = Instantiate(bolaNieve);
            bola.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y);
            bola.GetComponent<Rigidbody2D>().AddForce(new Vector2(625f, 500f));
        }
        else
        {
            GameObject bola = Instantiate(bolaNieve);
            bola.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y);
            bola.GetComponent<Rigidbody2D>().AddForce(new Vector2(-625f, 500f));
        }
    }//Recibes daño, haces un invoke para que la animacion vuelcva a su estado original
    void RecibirDano()
    {
        this.dolor = true;
        Invoke("RecuperarSubzero", 0.3f);
        //Sonido;
        hitSource.PlayOneShot(hit);
        //Si no estas bloqueando
        if (!this.bloquear)
        {   //A tu vida se le resta el ataque del enemigo
            vida -= scorpionMuneco.gameObject.GetComponent<ScorpionController>().ataque;
            //Instanciar sangre
            Sangrar();     
            //Si has muerto pone la vida a 0 y se activan varias bools para poder hacer el finish him y fatality
            if (this.vida <= 0)
            {
                this.vida = 0;
                muerto = true;
                noMover = true;
                if (suelo == false)
                {
                    Invoke("Levantarse", 2.5f);
                }
                else
                {
                    hitSource.PlayOneShot(scorpionWins);
                    levantar = false;
                    eventfinishStopSubzero();
                }
                suelo = true;
            }//Llamar al delegado de perder vida para poder bajar la barra de vida
            if (eventPerderVidaSubzero != null)
            {
                eventPerderVidaSubzero();
            }       
        }
    }//Cuando muere despues de estar en el suelo se levante y llama al delegado para poder poner en la pantalla el texto de finish him
    private void Levantarse()
    {
        levantar = true;
        mareos = true;
        muerto = false;
        eventfinishSubzero();
    }
    //Quitar la animacion de dolor
    private void RecuperarSubzero()
    {
        this.dolor = false;
    }//Impulso en la direccion contraria a la del personaje cuando recibe un golpe
    private void ImpulsoHit()
    {
        if (!this.bloquear)
        {
            if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300f, 150f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(300f, 150f));
            }
        }
    }

    IEnumerator MinCombo(float f, int st)
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(f);
        Debug.Log("MIN");
        if (!cBreak)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(f);
        Debug.Log("MAX");
        if (status == st)
        {
            status = 0;
        }
    }//Combo instancia una circulo de hielo que congela en la direccion a la que mira el personaje
    private void Combo()
    {
        if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
        {
            GameObject fuego = Instantiate(hieloCombo);
            fuego.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y);
        }
        else
        {
            GameObject fuego = Instantiate(hieloCombo);
            fuego.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y);
        }
    }//Instancia un disparo que sirve para hacer el fatality
    private void AtaqueFatality()
    {
        if (scorpionMuneco.gameObject.GetComponent<ScorpionController>().cambio)
        {
            GameObject bola = Instantiate(hieloFatality);
            bola.GetComponent<Hielo>().vel = 7f;
            bola.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
        }
        else
        {
            GameObject bola = Instantiate(hieloFatality);
            bola.GetComponent<Hielo>().vel = -7f;
            bola.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
        }
    }
    //Instancia sangre
    private void Sangrar()
    {
        GameObject particulas = Instantiate(sangre);
        particulas.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 0.5f);
        Destroy(particulas.gameObject, 3f);
    }
}

