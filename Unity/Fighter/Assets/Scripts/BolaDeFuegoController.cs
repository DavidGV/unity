﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaDeFuegoController : MonoBehaviour
{
    public float vel;
    void Start()
    {   //Destrozar objeto en 5s
        Destroy(this.gameObject, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        //Se mueve constante
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {   //Se elimina al colisionar con el enemigo
        if (collision.transform.tag == "Subzero" || collision.transform.tag == "S2" || collision.transform.tag == "I2")
        {
            Destroy(this.gameObject);
        }
    }
}
