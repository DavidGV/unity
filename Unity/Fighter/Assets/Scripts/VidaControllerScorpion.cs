﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaControllerScorpion : MonoBehaviour
{
    private GameObject pj;
    private Transform bar;
    private Transform bar2;
    private float vidaInicial;
    private float vidaActual;

    private void Start()
    {
        //Accedemos a la gameObject "Barra" y la vidaInicial=a la vida que tiene al principio el enemigo
        bar = transform.Find("Barra1");
        //bar2 servira para bajar la vida en rojo
        bar2 = transform.Find("Barra11");
        pj = GameObject.Find("Scorpion");
        vidaInicial = pj.gameObject.GetComponent<ScorpionController>().vida;
        //Se suscribe al delegado de perder vida para poder bajar la vida de la barra
        pj.GetComponent<ScorpionController>().eventPerderVidaScorpion += PonerMedida;
    }

    void Update()
    {

    }
    //Hacemos una regla de 3 para que la vida sea proporcional a la barra
    public void PonerMedida()
    {
        Debug.Log("BARRA DE VIDA");
        vidaActual = pj.gameObject.GetComponent<ScorpionController>().vida;
        float vidaPj = ((vidaActual * 100) / vidaInicial) * 0.01f;
        bar.localScale = new Vector3(vidaPj, 1f);
        //Invoke para bajar la vida roja     
        Invoke("PonerMedida2", 0.5f);
    }
    //Funcion para poner la vida roja en su sitio
    public void PonerMedida2()
    {
        float vidaPj = ((vidaActual * 100) / vidaInicial) * 0.01f;
        bar2.localScale = new Vector3(vidaPj, 1f);
    }
}
