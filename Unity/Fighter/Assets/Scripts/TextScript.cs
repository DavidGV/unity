﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScript : MonoBehaviour
{   //GameObjects
    public GameObject fight;
    public GameObject fatality;
    public GameObject finish;
    public GameObject munecoScorpion;
    public GameObject munecoSubzero;
    //Sonido
    public AudioClip fatalitySong;
    private AudioSource SourceMusic;
    public AudioClip fightSound;
    public AudioClip finishSong;
    public AudioClip scorpionWins;
    public AudioClip subzeroWins;

    void Start()
    {   //GameObjects.Find
        munecoScorpion = GameObject.Find("Scorpion");
        munecoSubzero = GameObject.Find("SubZero");
        //Suscripciones a eventos
        munecoScorpion.GetComponent<ScorpionController>().eventfinishScorpion += Finish;
        munecoScorpion.GetComponent<ScorpionController>().eventfinishStopScorpion += DesFinish;
        munecoScorpion.GetComponentInChildren<RecibirDanoScorpion>().eventFatalityScorpion1 += Fatality1; 
        munecoScorpion.GetComponentInChildren<RecibirDanoScorpion2>().eventFatalityScorpion2 += Fatality1;
        munecoSubzero.GetComponent<SubZeroController>().eventfinishSubzero += Finish;
        munecoSubzero.GetComponent<SubZeroController>().eventfinishStopSubzero += DesFinish;
        munecoSubzero.GetComponentInChildren<RecibirDanoSubzero>().eventFatalitySubzero1 += Fatality2;
        munecoSubzero.GetComponentInChildren<RecibirDanoSubzero2>().eventFatalitySubzero2 += Fatality2;
        //Activar texto fight
        fight.SetActive(true);
        //Invoke para desactivar texto a los 3s
        Invoke("DesFight", 3f);
        //Sonido
        SourceMusic = this.GetComponent<AudioSource>();
        SourceMusic.PlayOneShot(fightSound);
    }

    void Update()
    {

    }
    //Desctivar texto
    public void DesFight()
    {
        fight.SetActive(false);
    }
    //Desctivar texto finish y activar texto de Fatality
    public void Fatality1()
    {
        DesFinish();
        fatality.SetActive(true);
        SourceMusic.PlayOneShot(fatalitySong);
        Invoke("DesFatality1", 3);
    }
    public void DesFatality1()
    {
        SourceMusic.PlayOneShot(subzeroWins);
    }
    //Desctivar texto finish y activar texto de Fatality
    public void Fatality2()
    {
        DesFinish();
        fatality.SetActive(true);
        SourceMusic.PlayOneShot(fatalitySong);
        Invoke("DesFatality2", 3);
    }
    public void DesFatality2()
    {
        SourceMusic.PlayOneShot(scorpionWins);
    }
    //Activar texto
    public void Finish()
    {
        finish.SetActive(true);
        SourceMusic.PlayOneShot(finishSong);
    }
    //Desctivar texto
    public void DesFinish()
    {
        finish.SetActive(false);
    }
}
