﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecibirDanoScorpion2 : MonoBehaviour
{
    //Delegado para avisar de que pierdes vida
    public delegate void PerderVidaScorpionChild2();
    public event PerderVidaScorpionChild2 eventPerderVidaScorpionChild2;
    //Delegado para avisar de que estas congelado
    public delegate void HieloScorpionChild2();
    public event HieloScorpionChild2 eventHieloScorpionChild2;
    //Delegado para poner el texto de fatality
    public delegate void fatalityScorpion2();
    public event fatalityScorpion2 eventFatalityScorpion2;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   //Si colisionas con el golpe del enemigo, el disparo que congela o una piedra entras
        if (collision.gameObject.tag == "D2" || collision.gameObject.tag == "Hielo" || collision.gameObject.tag == "piedra")
        {
            if (eventPerderVidaScorpionChild2 != null)
            {   //Llamas al delegado de perder vida
                eventPerderVidaScorpionChild2();
            }
            //Si has colisionado con el hielo y no estas bloquando llamas al delegado para congelarte
            if (collision.gameObject.tag == "Hielo"){
               if(!this.GetComponentInParent<ScorpionController>().bloquear){
                    eventHieloScorpionChild2();
                }
            }
        }//Si has colisionado con el proyectil del fatality activas el bool de la animacion del fatality y tambien llamas al delegado para poner el texto
        else if (collision.gameObject.tag == "pFatalitySub")
        {
            this.GetComponentInParent<ScorpionController>().fatality = true;
            eventFatalityScorpion2();
        }
    }
}
