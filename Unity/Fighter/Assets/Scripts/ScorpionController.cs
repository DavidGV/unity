﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionController : MonoBehaviour
{   //Bools para movimientos y ataques
    private bool adelante = false;
    private bool atras = false;
    public bool salto = false;
    private bool agachado = false;
    private bool h = false;
    private bool m = false;
    private bool l = false;
    private bool canJump = true;
    //Floats de velocidades
    public float velAdelante = 5f;
    public float velAtras = -3f;
    public float velAdelanteTemp = 5f;
    public float velAtrasTemp = -3f;
    //Floats de fuerza para el salto
    public float jumpForce = 15f;
    //Ints de vida, ataque y cooldown
    public int vida = 300;
    public int ataque = 15;
    private int cooldown = 0;
    //GameObject Subzero
    public GameObject subZeroMuneco;
    //Bool para el cambio de lado
    public bool cambio = false;
    //Delegado para cambiar de lado al enemigo
    public delegate void cambioDeLado();
    public event cambioDeLado eventcambioDeLado;
    //Bools de animaciones
    public bool dolor = false;
    public bool bloquear = false;
    public bool muerto = false;
    public bool mareos = false;
    public bool levantar = false;
    public bool fatality = false;
    public bool suelo = false;
    private bool pared = false;
    //GameObjects de proyectiles
    public GameObject bolaDeFuego;
    public GameObject bolaDeFuegoAeria;
    public GameObject bolaBaja;
    public GameObject fuegoCombo;
    public GameObject fuegoFatality;
    //GameObject de particulas de sangre
    public GameObject sangre;
    //Status y breack de las corrutinas 
    public int status;
    private bool cBreak;
    //Delegado para que la barra de vida se actualize
    public delegate void PerderVidaScorpion();
    public event PerderVidaScorpion eventPerderVidaScorpion;
    //Delegado para poner el texto de finish
    public delegate void finishScorpion();
    public event finishScorpion eventfinishScorpion;
    //Delegado para qitar el texto de finish
    public delegate void finishStopScorpion();
    public event finishStopScorpion eventfinishStopScorpion;
    //Bool de estar congelado
    public bool congelado;
    //Sonido
    public AudioClip hit;
    public AudioSource hitSource;
    public AudioClip subzeroWins;
    public AudioClip fatalityGrito;

    void Start()
    {   
        //cambio poner en false
        cambio = false;
        //buscar subzero
        subZeroMuneco = GameObject.Find("SubZero");
        //Suscripciones de los delegados de tus hijos para recibir daño, impulsarte con el golpe y congelarte
        this.GetComponentInChildren<RecibirDanoScorpion>().eventPerderVidaScorpionChild1 += RecibirDano;
        this.GetComponentInChildren<RecibirDanoScorpion2>().eventPerderVidaScorpionChild2 += RecibirDano;
        this.GetComponentInChildren<RecibirDanoScorpion>().eventPerderVidaScorpionChild1 += ImpulsoHit;
        this.GetComponentInChildren<RecibirDanoScorpion2>().eventPerderVidaScorpionChild2 += ImpulsoHit;
        this.GetComponentInChildren<RecibirDanoScorpion>().eventHieloScorpionChild1 += Hielo;
        this.GetComponentInChildren<RecibirDanoScorpion2>().eventHieloScorpionChild2 += Hielo;
        //Sonido
        hitSource = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        //Bools de animaciones
        this.GetComponent<Animator>().SetBool("Adelante", adelante);
        this.GetComponent<Animator>().SetBool("Atras", atras);
        this.GetComponent<Animator>().SetBool("Salto", salto);
        this.GetComponent<Animator>().SetBool("Agachado", agachado);
        this.GetComponent<Animator>().SetBool("H", h);
        this.GetComponent<Animator>().SetBool("M", m);
        this.GetComponent<Animator>().SetBool("L", l);
        this.GetComponent<Animator>().SetBool("Dolor", dolor);
        this.GetComponent<Animator>().SetBool("Bloquear", bloquear);
        this.GetComponent<Animator>().SetBool("Congelado", congelado);
        this.GetComponent<Animator>().SetBool("Muerto", muerto);
        this.GetComponent<Animator>().SetBool("Levantar", levantar);
        this.GetComponent<Animator>().SetBool("Fatality", fatality);

        //Rotar el personaje, cambiar las velocidades y llamar al delegado de cambio de lado para que el enemigo tambien se cambie de lado
        if (this.transform.position.x - subZeroMuneco.transform.position.x > 0 && !cambio)
        {
            cambio = true;
            bolaDeFuego.transform.Rotate(0, 180, 0);
            this.transform.Rotate(0, 180, 0);
            velAdelante = -velAtrasTemp;
            velAtras = -velAdelanteTemp;
            eventcambioDeLado();

        }
        else if (this.transform.position.x - subZeroMuneco.transform.position.x < 0 && cambio)
        {
            cambio = false;
            bolaDeFuego.transform.Rotate(0, 180, 0);
            this.transform.Rotate(0, 180, 0);
            velAdelante = velAdelanteTemp;
            velAtras = velAtrasTemp;
            eventcambioDeLado();
        }
        //Mover a la derecha
        if (Input.GetKey("d") && !congelado)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(velAdelante, this.GetComponent<Rigidbody2D>().velocity.y);
            if (!cambio)
            {
                adelante = true;
                atras = false;
            }
            else
            {
                adelante = false;
                atras = true;
            }

        }//Mover a la izquierda
        else if (Input.GetKey("a") && !congelado)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(velAtras, this.GetComponent<Rigidbody2D>().velocity.y);
            if (!cambio)
            {
                adelante = false;
                atras = true;
            }
            else
            {
                adelante = true;
                atras = false;
            }

        }//Quedarte quieto
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            adelante = false;
            atras = false;
        }//saltar
        if (Input.GetKeyDown("w") && canJump && !congelado)
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            canJump = false;
            salto = true;
            agachado = false;

        }//Agacharte, al agacharte no te puedes mover horizontalmente
        else if (Input.GetKey("s") && !congelado)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            adelante = false;
            atras = false;
            agachado = true;
        }//Ponerte de pie
        else
        {
            agachado = false;
        }//Golpe L, tambien inicia combo, en el caso de que el enemigo este con el bool activo de mareos empieza otro combo
        if (Input.GetKeyDown("c") && !congelado)
        {
            l = true;
            cBreak = false;
            if (subZeroMuneco.gameObject.GetComponent<SubZeroController>().mareos)
            {
                StartCoroutine(MinCombo(0f, 1));
                StartCoroutine(MaxCombo(1f, status));
            }
            else
            {
                StartCoroutine(MinCombo(0.25f, 1));
                StartCoroutine(MaxCombo(1.5f, status));
            }
        }//Quita el golpe L
        else
        {
            l = false;
        }//Golpe M, si has hecho bien el combo llama a la funcion Combo(), en el caso de que el enemigo este con el bool activo de mareos continua con el combo
        if (Input.GetKeyDown("v") && !congelado)
        {
            if (subZeroMuneco.gameObject.GetComponent<SubZeroController>().mareos)
            {
                if (status == 1)
                {
                    StartCoroutine(MinCombo(0f, 2));
                    StartCoroutine(MaxCombo(1f, status));
                }
                else
                {
                    m = true;
                    cBreak = true;
                }
                
            }
            else
            {
                if (status == 2)
                {
                    Combo();
                    status = 0;
                }
                else
                {
                    m = true;
                    cBreak = true;
                }
            }
        }//Quita el golpe M
        else
        {
            m = false;
        }//Golpe H, continua con el combo si es que lo habias iniciado, en el caso de que el enemigo este con el bool activo de mareos puedes finalizar el combo llamando a la funcion AtaqueFatality()
        if (Input.GetKeyDown("b") && !congelado)
        {
            if (subZeroMuneco.gameObject.GetComponent<SubZeroController>().mareos)
            {
                if (status == 2)
                { 
                    AtaqueFatality();
                }
                else
                {
                    h = true;
                    cBreak = true;
                }
            }
            else
            {
                if (status == 1)
                {
                    h = true;
                    StartCoroutine(MinCombo(0.25f, 2));
                    StartCoroutine(MaxCombo(1.5f, status));
                }
                else
                {
                    h = true;
                    cBreak = true;
                }

            }
        }//Quita el golpe H
        else
        {
            h = false;
        }//Bloqueas golpes pero no puedes moverte horizontalmente
        if (Input.GetKey("f") && !congelado)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            bloquear = true;
        }//Dejas de bloquear
        else
        {
            bloquear = false;
        }//Lanzas un proyectil, si solo pulsas "g" el disparo va recto, si ademas vas hacia delante es un tiro aerio con parabola y en el caso de que te agaches tira una bola que va rebotando en el suelo
        if (Input.GetKeyDown("g") && !congelado)
        {   //Si puedes disparar
            if (cooldown==50) { 
                if (adelante)
                {
                    DisparoAerio();
                }
                else if (agachado)
                {
                    DisparoBajo();
                }
                else
                {
                    Disparo();
                }
                cooldown = 0;
            }

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {   //Si colisionas con el suelo puedes volver a saltar
        if (collision.gameObject.tag == "Suelo")
        {
            canJump = true;
            salto = false;
            pared = false;
        }//Si colisionas con la pared y estas saltando activas el bool
        if (collision.gameObject.tag == "Pared" && salto)
        {
            pared = true;
            this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }

    }
    private void FixedUpdate()
    {   //Actualizar el cooldown de disparo
        if (cooldown < 50)
        {
            cooldown++;
        }
    }
    private void LateUpdate()
    {
        //Si estas estas en la pared mientras saltas bajas 
        if (pared)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    IEnumerator MinCombo(float f, int st)
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(f);
        Debug.Log("MIN");
        if (!cBreak)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(f);
        Debug.Log("MAX");
        if (status == st)
        {
            status = 0;
        }
    }
    //Instancia un disparo horizontal en la direcciona a la que mira el personaje
    void Disparo()
    {
        if (cambio)
        {
            GameObject bola = Instantiate(bolaDeFuego);
            bola.GetComponent<BolaDeFuegoController>().vel = -7f;
            bola.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y);
        }
        else
        {
            GameObject bola = Instantiate(bolaDeFuego);
            bola.GetComponent<BolaDeFuegoController>().vel = 7f;
            bola.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y);
        }
    }//Instancia un disparo aerio con parabola en la direcciona a la que mira el personaje
    void DisparoAerio()
    {
        if (cambio)
        {
            GameObject bola = Instantiate(bolaDeFuegoAeria);
            bola.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y);
            bola.GetComponent<Rigidbody2D>().AddForce(new Vector2(-625f, 500f));
        }
        else
        {
            GameObject bola = Instantiate(bolaDeFuegoAeria);
            bola.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y);
            bola.GetComponent<Rigidbody2D>().AddForce(new Vector2(625f, 500f));
        }
    }//Instancia un disparo bajo que va rebotando en el suelo en la direcciona a la que mira el personaje
    void DisparoBajo()
    {
        if (cambio)
        {
            GameObject bola = Instantiate(bolaBaja);
            bola.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y);
            bola.GetComponent<Rigidbody2D>().AddForce(new Vector2(-400f, -300f));
        }
        else
        {
            GameObject bola = Instantiate(bolaBaja);
            bola.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y);
            bola.GetComponent<Rigidbody2D>().AddForce(new Vector2(400f, -300f));
        }
    }//Combo instancia una llama de fuego en la direccion a la que mira el personaje
    void Combo()
    {
        if (cambio)
        {
            GameObject fuego = Instantiate(fuegoCombo);
            fuego.transform.position = new Vector2(this.transform.position.x - 2f, this.transform.position.y + 0.5f);
        }
        else
        {
            GameObject fuego = Instantiate(fuegoCombo);
            fuego.transform.position = new Vector2(this.transform.position.x + 2f, this.transform.position.y + 0.5f);
        }

    }//Recibes daño, haces un invoke para que la animacion vuelcva a su estado original
    void RecibirDano()
    {
        this.dolor = true;
        Invoke("RecuperarScorpion", 0.3f);
        congelado = false;
        //Sonido
        hitSource.PlayOneShot(hit);
        //Si no estas bloqueando
        if (!this.bloquear)
        {   //A tu vida se le resta el ataque del enemigo
            this.vida -= subZeroMuneco.gameObject.GetComponent<SubZeroController>().ataque;
            //Instanciar sangre
            Sangrar();
            //Si has muerto pone la vida a 0 y se activan varias bools para poder hacer el finish him y fatality
            if (this.vida <= 0)
            {
                this.vida = 0;
                muerto = true;
                congelado = true;
                if (suelo == false)
                {
                    Invoke("Levantarse", 2.5f);
                }
                else
                {
                    hitSource.PlayOneShot(subzeroWins);
                    levantar = false;
                    eventfinishStopScorpion();
                }
                suelo = true;
            } //Llamar al delegado de perder vida para poder bajar la barra de vida
            if (eventPerderVidaScorpion != null)
            {
                eventPerderVidaScorpion();
            }



        }
    }//Cuando muere despues de estar en el suelo se levante y llama al delegado para poder poner en la pantalla el texto de finish him
    private void Levantarse()
    {
        levantar = true;
        mareos = true;
        muerto = false;
        eventfinishScorpion();
    }//Quitar la animacion de dolor
    private void RecuperarScorpion()
    {
        this.dolor = false;
    }//Impulso en la direccion contraria a la del personaje cuando recibe un golpe
    private void ImpulsoHit()
    {
        if (!this.bloquear)
        {
            if (cambio)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(300f, 150f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300f, 150f));
            }
        }
    }
    //Cuando estas congelado no puedes moverte y se cambia el color del personaje
    private void Hielo()
    {
        congelado = true;
        this.GetComponent<SpriteRenderer>().color = new Color(0 / 255f, 152 / 255f, 255 / 255f);
        Invoke("Deshielo", 3);
    }
    //El personaje vuele a poder moverse y tambien vuelve su color
    private void Deshielo()
    {
        congelado = false;
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }
    //Instancia un disparo que sirve para hacer el fatality
    private void AtaqueFatality()
    {
        if (cambio)
        {
            GameObject bola = Instantiate(fuegoFatality);
            bola.GetComponent<BolaDeFuegoController>().vel = -7f;
            bola.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
        }
        else
        {
            GameObject bola = Instantiate(fuegoFatality);
            bola.GetComponent<BolaDeFuegoController>().vel = 7f;
            bola.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
        }
    }
    //Instancia sangre
    private void Sangrar()
    {
        GameObject particulas = Instantiate(sangre);
        particulas.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 0.5f);
        Destroy(particulas.gameObject, 3f);
    }
}
