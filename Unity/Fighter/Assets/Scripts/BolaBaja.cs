﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaBaja : MonoBehaviour
{
    //Fuerzas X e Y
    private float fX = 10f;
    private float fY = 1000f;
    void Start()
    { 
        //Destrozar objeto en 5s
        Destroy(this.gameObject,5f);
    }
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si colisiona con el suelo rebota con menos fuerza tanto vertical como horizontal
        if (collision.transform.tag == "Suelo"){
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fX, fY));
            fX *= 0.85f;
            fY *= 0.85f;
        }//Si colisiona con el enemigo se destruye
        else if (collision.transform.tag == "S2" || collision.transform.tag == "I2")
        {
            Destroy(this.gameObject);
        }
        

        
    }
}
