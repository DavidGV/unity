﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecibirDanoScorpion : MonoBehaviour
{
    //Delegado para avisar de que pierdes vida
    public delegate void PerderVidaScorpionChild1();
    public event PerderVidaScorpionChild1 eventPerderVidaScorpionChild1;
    //Delegado para avisar de que estas congelado
    public delegate void HieloScorpionChild1();
    public event HieloScorpionChild1 eventHieloScorpionChild1;
    //Delegado para poner el texto de fatality
    public delegate void fatalityScorpion1();
    public event fatalityScorpion1 eventFatalityScorpion1;

    void Start()
    {
        
    }
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   //Si colisionas con el golpe del enemigo, el disparo que congela o una piedra entras
        if (collision.gameObject.tag == "D2" || collision.gameObject.tag == "Hielo" || collision.gameObject.tag == "piedra")
        {   
            if(eventPerderVidaScorpionChild1 != null)
            {   //Llamas al delegado de perder vida
                eventPerderVidaScorpionChild1();
            }
            //Si has colisionado con el hielo y no estas bloquando llamas al delegado para congelarte
            if (collision.gameObject.tag == "Hielo"){
                if(!this.GetComponentInParent<ScorpionController>().bloquear){
                    eventHieloScorpionChild1();
                }   
            }
        }//Si has colisionado con el proyectil del fatality activas el bool de la animacion del fatality y tambien llamas al delegado para poner el texto
        else if (collision.gameObject.tag == "pFatalitySub")
        {   
            this.GetComponentInParent<ScorpionController>().fatality = true;
            this.GetComponentInParent<ScorpionController>().hitSource.PlayOneShot(this.GetComponentInParent<ScorpionController>().fatalityGrito);
            eventFatalityScorpion1();
        }
    }
}
