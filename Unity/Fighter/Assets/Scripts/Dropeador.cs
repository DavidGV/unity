﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropeador : MonoBehaviour
{
    //Array de piedras
    public GameObject[] piedrasPrefs;

    void Start()
    {   //Llama a instanciar a los 3s y despues periodicamente cada 5s
        InvokeRepeating("Instanciar", 3, 5);
    }

    // Update is called once per frame
    void Update()
    {

    }
    //Instancia una piedra aleatoria
    void Instanciar()
    {
      Instantiate(piedrasPrefs[Random.Range(0, piedrasPrefs.Length - 1)]);  
    }
}
