﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hielo : MonoBehaviour
{
    public float vel;
    void Start()
    {   //Destrozar objeto en 5s
        Destroy(this.gameObject, 5f);
    }

    void Update()
    {   //Se mueve a una velocidad constante
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {   //Se destruye al colisionar con el enemigo
        if (collision.transform.tag == "S1" || collision.transform.tag == "I1")
        {
            Destroy(this.gameObject);
        }
    }
}
