﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiedraController : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {   //Se destruye cuando colisiona con el suelo o los personajes
        if (collision.transform.tag == "Suelo" || collision.transform.tag == "S1" || collision.transform.tag == "S2" || collision.transform.tag == "I1" || collision.transform.tag == "I2")
        {
            Destroy(this.gameObject);
        }
        
    }
}
