﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecibirDanoSubzero2 : MonoBehaviour
{
    //Delegado para avisar de que pierdes vida
    public delegate void PerderVidaSubzeroChild2();
    public event PerderVidaSubzeroChild2 eventPerderVidaSubzeroChild2;
    //Delegado para poner el texto de fatality
    public delegate void fatalitySubzero2();
    public event fatalitySubzero2 eventFatalitySubzero2;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   //Si colisionas con el golpe del enemigo o una piedra entras
        if (collision.gameObject.tag == "D1" || collision.gameObject.tag == "piedra")
        {
            if (eventPerderVidaSubzeroChild2 != null)
            {   //Llamas al delegado de perder vida
                eventPerderVidaSubzeroChild2();
            }
        }//Si has colisionado con el proyectil del fatality activas el bool de la animacion del fatality y tambien llamas al delegado para poner el texto
        else if (collision.gameObject.tag == "pFatality")
        {   
            this.GetComponentInParent<SubZeroController>().fatality = true;
            eventFatalitySubzero2();
        }
    }
}
