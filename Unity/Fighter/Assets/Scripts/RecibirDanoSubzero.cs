﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecibirDanoSubzero : MonoBehaviour
{
    //Delegado para avisar de que pierdes vida
    public delegate void PerderVidaSubzeroChild1();
    public event PerderVidaSubzeroChild1 eventPerderVidaSubzeroChild1;
    //Delegado para poner el texto de fatality
    public delegate void fatalitySubzero1();
    public event fatalitySubzero1 eventFatalitySubzero1;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   //Si colisionas con el golpe del enemigo o una piedra entras
        if (collision.gameObject.tag == "D1" || collision.gameObject.tag == "piedra")
        {
            if (eventPerderVidaSubzeroChild1 != null)
            {   //Llamas al delegado de perder vida
                eventPerderVidaSubzeroChild1();
            }
        }//Si has colisionado con el proyectil del fatality activas el bool de la animacion del fatality y tambien llamas al delegado para poner el texto
        else if (collision.gameObject.tag == "pFatality")
        {
            this.GetComponentInParent<SubZeroController>().fatality = true;
            this.GetComponentInParent<SubZeroController>().hitSource.PlayOneShot(this.GetComponentInParent<SubZeroController>().fatalityGrito);
            eventFatalitySubzero1();
        }
    }
    
}
