﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaDeNieve : MonoBehaviour
{
    void Start()
    {   //Destrozar objeto en 5s
        Destroy(this.gameObject, 5f);
    }

    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {   //Si colisiona con el enemigo se destruye pero no con sus proyectiles
        if (collision.transform.tag != "D1")
        {
            Destroy(this.gameObject);
        }
    }
}
