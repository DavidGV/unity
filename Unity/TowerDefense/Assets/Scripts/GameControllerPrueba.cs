﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class GameControllerPrueba : MonoBehaviour
{
    // Start is called before the first frame update
    public int contadorEnemigos = 1;
    public float timeSpawn;
    private float timeSpawnIni;
    public GameObject[] enemigos;
    public Transform[] targets;
    public int rondas=1;
    // public GameObject[] torres;
    public List<GameObject> torres = new List<GameObject>();
    public List<GameObject> enemyList = new List<GameObject>();
    private GameObject enemy;
    private static int crearEnemigo = 1;
    private GameObject end;
    public delegate void reiniciarTilemap2();
    public event reiniciarTilemap2 eventReiniciarTilemap2;
    public int countTorres=0;
    private void Awake()
    {
        end = GameObject.Find("FinalMuerto");
        timeSpawn = 1;
        timeSpawnIni = timeSpawn;
    }
    void Start()
    {
        //ESTE FOR ES MUY TEMPORAL, HAY QUE HACER QUE SE ASIGNE EL ID CUANDO SE INSTANCIE LA TORRE
        int i = 1;
        foreach (GameObject tower in torres)
        {
            tower.GetComponent<Torre>().ID = i;
            i++;
        }
        InvokeRepeating("BajarCoolDown", 0.2f, 2f);
        InvokeRepeating("instanciarEnemigo", 0, timeSpawn);
        InvokeRepeating("subTowers", 0, 0.2f);
       // InvokeRepeating("subEnemys", 0, 0.7f);
    }


    public void instanciarEnemigo()
    {
       
        switch (crearEnemigo)
        {
            case 1:
                enemy = Instantiate(enemigos[Random.Range(0, enemigos.Length - 1)]);
                if (enemy.GetComponent<AIDestinationSetter>() != null)
                {
                    enemy.GetComponent<AIDestinationSetter>().target = targets[0];
                    enemy.GetComponent<AIDestinationSetter>().target2 = targets[4];
                }
                else
                {
                    enemy.GetComponent<AIDestiantionSetter2>().target = targets[0];
                    enemy.GetComponent<AIDestiantionSetter2>().target2 = targets[4];
                }
                enemyList.Add(enemy);
                enemy.transform.position = new Vector2(4.95f, -6.24f);
                crearEnemigo++;
                break;
            case 2:
                enemy = Instantiate(enemigos[Random.Range(0, enemigos.Length - 1)]);
                if (enemy.GetComponent<AIDestinationSetter>() != null)
                {
                    enemy.GetComponent<AIDestinationSetter>().target = targets[1];
                    enemy.GetComponent<AIDestinationSetter>().target2 = targets[4];
                }
                else
                {
                    enemy.GetComponent<AIDestiantionSetter2>().target = targets[1];
                    enemy.GetComponent<AIDestiantionSetter2>().target2 = targets[4];
                }
                enemy.transform.position = new Vector2(6.5f, -5.46f);
                crearEnemigo++;
                break;
            case 3:
                enemy = Instantiate(enemigos[Random.Range(0, enemigos.Length - 1)]);
                if (enemy.GetComponent<AIDestinationSetter>() != null)
                {
                    enemy.GetComponent<AIDestinationSetter>().target = targets[2];
                    enemy.GetComponent<AIDestinationSetter>().target2 = targets[4];
                }
                else
                {
                    enemy.GetComponent<AIDestiantionSetter2>().target = targets[2];
                    enemy.GetComponent<AIDestiantionSetter2>().target2 = targets[4];
                }
                enemy.transform.position = new Vector2(8.36f, -4.48f);
                crearEnemigo++;
                break;
            case 4:
                enemy = Instantiate(enemigos[Random.Range(0, enemigos.Length - 1)]);
                if (enemy.GetComponent<AIDestinationSetter>() != null)
                {
                    enemy.GetComponent<AIDestinationSetter>().target = targets[3];
                    enemy.GetComponent<AIDestinationSetter>().target2 = targets[4];
                }
                else
                {
                    enemy.GetComponent<AIDestiantionSetter2>().target = targets[3];
                    enemy.GetComponent<AIDestiantionSetter2>().target2 = targets[4];
                }
                enemyList.Add(enemy);
                enemy.transform.position = new Vector2(10.2f, -3.63f);
                crearEnemigo = 1;
                break;
            default:
                break;
        }
        enemy.GetComponent<Enemigo>().ID = contadorEnemigos;
        subEnemys(enemy);
        contadorEnemigos++;

    }
    void subTowers()
    {
        countTorres = torres.Count;
        foreach (GameObject tower in torres)
        {
            //Subscribes todas las torres instanciadas al evento que salta cuando proyectil reenvia a una torre basica a la clase padre torre que lo reenvia aqui, es decir Proyectil->Torre Hija -> Torre Padre ->GameController
            tower.GetComponent<Torre>().enemyErasedTowerEvent += Delete;
            end.GetComponent<FinalMuertoController>().enemyArrivedEvent += Delete;
        }
    }
    void subEnemys(GameObject enemy)
    {
        
            //subscribes al enemigo a un posible borrado de la torre
            if (enemy.GetComponent<AIDestinationSetter>() != null)
            { 
            enemy.GetComponent<Enemigo>().towerErasedEvent += DeleteTower;
            }
            end.GetComponent<FinalMuertoController>().enemyArrivedEvent += DeleteEnemy;
            // Debug.Log(enemy.GetComponent<Enemigo>().ID);
        
    }
    void Delete(GameObject tarjet)
    {
        // Debug.Log("Dentro de delete");
        /*print("ANTES");
        BarraTienda.dinero += 0.1f;
        print("DESPUES");
        if (BarraTienda.dinero > 10f)
        {
            BarraTienda.dinero = 10f;
        }*/
        foreach (GameObject tower in torres)
        {
            if (tower.GetComponent<Torre>().tarjets.Contains(tarjet))
            {

                if (tower.GetComponent<Torre>().tarjet.GetComponent<Enemigo>().ID == tarjet.GetComponent<Enemigo>().ID)
                {
                    tower.GetComponent<Torre>().tarjets.Remove(tarjet);
                    tower.GetComponent<Torre>().NextTarget();
                }
                else
                    tower.GetComponent<Torre>().tarjets.Remove(tarjet);
            }
            // tower.GetComponent<Torre>().enemyErasedTowerEvent += Delete;
        }
        enemyList.Remove(tarjet);
        Destroy(tarjet);
    }
    public void DeleteTower(GameObject targetTower)
    {
        //cambiar enemyList por el targetTower.getComponentInChildren<Torre>().tarjets
        foreach (GameObject enemy in enemyList)
        {
            if (enemy.GetComponent<Enemigo>().targetTower != null)
            {
                if (enemy.GetComponent<Enemigo>().targetTower.GetComponentInChildren<Torre>().ID == targetTower.GetComponentInChildren<Torre>().ID)
                {
                    //Debug.Log("entra en el if del ANTIGUO EQUALS");
                    enemy.GetComponent<Enemigo>().targetTower = null;
                    enemy.GetComponent<AIDestinationSetter>().targetTorre = false;
                }
            }
        }
        GameObject invent = targetTower.transform.GetChild(0).gameObject;
        torres.Remove(invent);
        targetTower.GetComponentInParent<SpriteRenderer>().enabled = false;
        Destroy(targetTower);
        eventReiniciarTilemap2();
    }

    void DeleteEnemy(GameObject enemy)
    {
        enemyList.Remove(enemy);
    }
    public void BajarCoolDown()
    {
        if (timeSpawn < 0.4)
        {
            StartCoroutine("nuevaRonda"); 
            
           
        }
        else { 
        this.timeSpawn -= 0.1f;
            CancelInvoke("instanciarEnemigo");
            InvokeRepeating("instanciarEnemigo", 0, timeSpawn);
        }

    }
    private IEnumerator nuevaRonda()
    {
        
        yield return new WaitForSeconds(10);//10 segundos de ronda a saco
        CancelInvoke("instanciarEnemigo");//20 secs de descanso
        yield return new WaitForSeconds(20);
        timeSpawn = timeSpawnIni;
        InvokeRepeating("instanciarEnemigo", 0, timeSpawn);
    }
   
}
