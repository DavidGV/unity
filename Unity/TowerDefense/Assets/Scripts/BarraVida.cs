﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BarraVida : MonoBehaviour
{
    public static float vida = 500f;
    private Transform bar;
    private float hpInicial;
    public GameObject FinalMuerto;
    // Start is called before the first frame update
    void Start()
    {
        FinalMuerto.GetComponent<FinalMuertoController>().eventBajarVida += PonerMedida;
        hpInicial = vida;
        bar = transform.Find("BarraVidaBase");
        //InvokeRepeating("PonerMedida", 0f, 1f);
    }
   
    private void PonerMedida()
    {
        float nivel = ((vida * 100) / this.hpInicial) * 0.01f;
        bar.localScale = new Vector3(nivel, 1f, bar.localScale.z);
    }
}
