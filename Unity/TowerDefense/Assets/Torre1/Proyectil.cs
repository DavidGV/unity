﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{

    public float atack;
    //public Vector2 position;
    public bool splash; //No implementado
    public GameObject tarjet;
    public GameObject torrePadre;
    public float sp;

    public delegate void enemyErased(GameObject tarjet);
    /// <summary>
    ///Este evento avisa a la torre que el target fue eliminado
    /// </summary>
    public event enemyErased enemyErasedEvent;


    // Update is called once per frame
    void FixedUpdate()
    {

        if (tarjet != null)
        {
            if (tarjet.GetComponent<Enemigo>().currentLife > 0)
            {
                //Esta linea de codigo usa un metodo de Vector3 para hacer que persiga al enemigo. Conocida gracias a truño y ruben
                this.transform.position = Vector3.MoveTowards(this.transform.position, tarjet.transform.position, sp);
            }
        }
        else Destroy(this.gameObject);

    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.gameObject.GetComponent<Enemigo>().ID == tarjet.GetComponent<Enemigo>().ID)
            {
                other.gameObject.GetComponent<Enemigo>().currentLife -= this.atack;
                if (other.gameObject.GetComponent<Enemigo>().currentLife <= 0)
                {
                    //Nota para Marc-Sensei, aqui no se borra el target, se borra en el GameControllerPrueba
                    enemyErasedEvent(tarjet);
                    //Proyectil->Torre Hija -> Torre Padre ->GameController
                }
            }
            Destroy(this.gameObject);
        }
        
    }


}


