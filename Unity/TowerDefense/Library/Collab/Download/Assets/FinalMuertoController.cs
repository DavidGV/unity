﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalMuertoController : MonoBehaviour
{
    public delegate void enemyArrived(GameObject enemy);
    public event enemyArrived enemyArrivedEvent;
    public delegate void bajarVida();
    public event bajarVida eventBajarVida;
    public int contador = 0;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("SaltaTrigger");
            enemyArrivedEvent(collision.gameObject);
            BarraVida.vida -= collision.gameObject.GetComponent<Enemigo>().atack;
            if (BarraVida.vida < 0)
            {
                BarraVida.vida = 0;
            }
            eventBajarVida();
            contador++;
            Debug.Log(contador); 
        }
    }
}
