﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorCorazones : MonoBehaviour
{
    public float salud;
    public Image[] corazones;
    public Sprite cEntero;
    public Sprite cMedio;
    public Sprite cVacio;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Actualizamos la vida del player y hacemos un Invoke que muestra los corazones
        salud = PlayerControler.vida;
        Invoke("ActualizarVida", 0.3f);

    }
    //La funcion recorre la lista corazones y dependiendo de la vida que tienes te pone el corazon vacio, medio o entero en la posicion que toque
    void ActualizarVida()
    {
        for (int i = 0; i < corazones.Length; i++)
        {
            if (i < salud && salud < i + 1)
            {
                corazones[i].sprite = cMedio;
            }
            else if (i < salud)
            {
                corazones[i].sprite = cEntero;
            }
            else
            {
                corazones[i].sprite = cVacio;

            }
        }
    }
}
