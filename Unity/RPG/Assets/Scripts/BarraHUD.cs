﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraHUD : MonoBehaviour
{
    public PlayerControler player;
    private Transform bar;
    private float xpInicial;
    private float xpActual;
    public static double nivelPor;

    private void Start()
    {
        //Accedemos a la gameObject "BarraNivel" y la xpInicial=a la xp que tiene al principio el player
        bar = transform.Find("BarraNivel");
        this.xpInicial = PlayerPrefs.GetFloat("XpNivel");
    }

    // Update is called once per frame
    void Update()
    {
        //Vamos revisando la xp
        this.xpActual = PlayerPrefs.GetFloat("XpActual"); 
        Invoke("PonerMedida", 0.3f);
    }
    public void PonerMedida()
    {
        //Si subes de nivel le resta la experienca a la inical y lo que sobre se queda para el proximo nivel
        //Aumentamos la xp inicial para que necesites mas xp para poder subir de nivel
        //Aumentamos el numero indicador del nivel
        //Multiplicamos el ataque del player *1.2
        if (xpActual>= xpInicial)
        {
            this.xpActual -= this.xpInicial;
            PlayerControler.xpActual = this.xpActual;
            this.xpInicial += 10f;
            PlayerControler.xpNivel = this.xpInicial;
            PlayerControler.nivel++;
            PlayerControler.ataque *= 1.2f;
        }
        //Hacemos una regla de 3 para que la xp sea proporcional a la barra
        float nivel = ((this.xpActual * 100) / this.xpInicial) * 0.01f;
        bar.localScale = new Vector3(nivel, 1f, bar.localScale.z);

        //Parsear un float a doble para poder poner el numero del % de la experiencia
        String value = nivel+"";
        double d = Double.Parse(value);
        nivelPor = Math.Truncate(Double.Parse(value) * 100);
    }
}
