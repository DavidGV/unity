﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {   //seguimiento de la HUD a la camara
        this.transform.position = new Vector3(
            camera.transform.position.x,
            camera.transform.position.y,
            camera.transform.position.z-100);
        
    }
}
