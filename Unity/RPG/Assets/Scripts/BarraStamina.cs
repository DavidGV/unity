﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraStamina : MonoBehaviour
{
    public GameObject bar2;
    private Transform bar;
    private float stInicial = 1000f;
    private float stActual = 1000f;
    public static bool gastar = false;
    public static bool cero = false;

    private void Start()
    {
        //Accedemos a la gameObject "BarraStamina"
        bar = transform.Find("BarraStamina");
    }

    // Update is called once per frame
    void Update()
    {
        PonerMedida();
    }
    private void FixedUpdate()
    {
        //Cundo gastas stamina le resta 5 por iteracion, cuando llega a cero cambia el color de la barra y activa el bool cero que no deja gastar mas
        if (gastar)
        {
            if (stActual > 0)
            {
                stActual -= 5;
            }
            else
            {
                Color color = new Color(200 / 255f, 200 / 255f, 43 / 255f);
                bar2.GetComponent<SpriteRenderer>().color = color;
                cero = true;
            }
        }//Si no gastas stamina le suma 3, en el caso de que la stamina llega a su valor maximo le devuelve el color y pone el bool cero en false
        else
        {
            if(stActual< stInicial)
            {
                stActual += 3;
            }
            else
            {
                stActual = stInicial;
                Color color = new Color(255 / 255f, 255 / 255f, 0 / 255f);
                bar2.GetComponent<SpriteRenderer>().color = color;
                cero = false;
            }
            
        }
    }
    public void PonerMedida()
    {
        //Hacemos una regla de 3 para que la stamina sea proporcional a la barra
        float nivel = ((this.stActual * 100) / this.stInicial) * 0.01f;
        bar.localScale = new Vector3(nivel, 1f, bar.localScale.z);
    }

}
