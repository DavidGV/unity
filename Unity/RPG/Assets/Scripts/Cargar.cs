﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Cargar : MonoBehaviour
{
    public Button cargar;
    
    void Start()
    {
        //Cuando pulsas el boton de cargar llama al TaskOnClick
        Button btn1 = cargar.GetComponent<Button>();
        cargar.onClick.AddListener(TaskOnClick);
    }
    //Carga la del archivo que previamente hemos guardado
    //Carga las variables importantes
    void TaskOnClick()
    {
       var partida = persistenciaPartida.CreateFromJSON(System.IO.File.ReadAllText(@".\Assets\Save\datosGuardado.json"));
        SceneManager.LoadScene(partida.escena);
        PlayerControler controler = new PlayerControler();
        PlayerControler playerControler = Instantiate(controler);
        playerControler.transform.position = new Vector2(partida.coordenadaX, partida.coordenadaY);
        PlayerControler.vida = partida.vida;
        PlayerControler.ataque = partida.ataque;
        PlayerControler.xpActual = partida.xpActual;
        PlayerControler.xpNivel = partida.xpNivel;
        PlayerControler.rupias = partida.rupias;
        PlayerControler.flechas = partida.flechas;
        PlayerControler.bombas = partida.bombas;
    }
}
