﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Empieza el juego en la escena casaInicial
    public void Jugar()
    {
        SceneManager.LoadScene("CasaInicial");
    }
    //Cierra el juego
    public void salir()
    {
        Application.Quit();
    }
    //Vuelve a la escena MenuPrincipal
    public void Salir()
    {
    SceneManager.LoadScene("MenuPrincipal");
    }
    //Desactiva el menu
    public void Continuar()
    {
        this.gameObject.SetActive(false);
        PlayerControler.menu = false;
        PlayerControler.noMover = false;
    }

}
