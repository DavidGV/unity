﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JarronControler : MonoBehaviour
{
    public GameObject[] objetos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Instancia un objeto consumible random de el array de objetos
    public void Objeto()
    {
        GameObject objeto = Instantiate(objetos[Random.Range(0, objetos.Length)]);
        objeto.transform.position = this.transform.position;

    }
}
