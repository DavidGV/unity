﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombaExplosion : MonoBehaviour
{
    
    bool activo = false;
    bool playerDañado=true;
    bool enemigoDañado=true;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<PointEffector2D>().enabled=false;
        Invoke("activar", 2.5f);
        Invoke("destruccion" ,2.7f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        //Si el trigger entra en contacto con el player le quita una vida
        if(collision.CompareTag("Player")&&activo&&playerDañado)
        {
            PlayerControler.vida--;
            playerDañado=false;
        }
        //Le resta el ataque del player + 3 a la vida del enemigo
        if (collision.CompareTag("Enemigo")&&activo&&enemigoDañado)
        {
            collision.gameObject.GetComponent<Enemigo>().vida -= PlayerControler.ataque+3f;
            enemigoDañado=false;
        }
      
        
    }
    
    //Activa el efector de la bomba y tambien activa la bomba
    public void activar()
    {
        activo = true;
        this.GetComponent<PointEffector2D>().enabled=true;
    }
    //Destruye la bomba
    public void destruccion(){
        Destroy(this.gameObject);   
        Destroy(this);
    }
}
