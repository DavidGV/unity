﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour
{
    // Start is called before the first frame update
    public float vel;
    public float velY;
   // public Scene[] scenes;
    private float velActual;
    private float velYActual;
    public bool arriba = false;
    public bool abajo = false;
    public bool derecha = false;
    public bool izquierda = false;
    
    private bool diag = false;
    private bool espada = false;
    private int cooldown = 0;
    private Animator anim;
    

    //Valores Link
    public float vida = 8f;
    public float ataque = 1f;
    public float xp;

    static GameObject link = null;
    public static bool mapa0vuelta = false;
    public static bool mazmorra01vuelta = false;
    public static bool mazmorra02vuelta = false;
    public static bool mazmorra03vuelta = false;
    public static bool mazmorra04vuelta = false;
    public static bool mazmorra041vuelta = false;
    public static bool mazmorra05vuelta = false;
    public static bool mazmorra06vuelta = false;
    public static bool mazmorra1vuelta = false;
    public static bool mazmorra2vuelta = false;
    public static bool mazmorra41 = false;
    public static bool mazmorra6vuelta = false;
    public static bool mazmorra61vuelta = false;

    //consumibles
    public int rupias = 0;
    public int flechas = 0;
    public int bombas = 0;
    //delegado a consumibles
   // public Consumibles consumibles;
    public delegate int rupiasCambio();
    public event rupiasCambio onRupiasCambio;

    void Awake()
    {
        //Patró singleton
        if (link == null)
        {
            //crea el objecte en el primer moment
            link = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(link);
        }
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
        string nombreMapa = Application.loadedLevelName;
        //Debug.Log("Mapa: " + nombreMapa);
        switch (nombreMapa)
        {
            case "Mapa0":
                if (mapa0vuelta)
                {
                    link.transform.position = new Vector2(-13.4f, 1.42f);
                }
                else
                {
                    link.transform.position = new Vector2(3.49f, -2.23f);
                }
                break;
            case "Mazmorra0":
                if (mazmorra01vuelta)
                {
                    link.transform.position = new Vector2(-24.45502f, 4.15f);
                }
                else if (mazmorra02vuelta)
                {
                    link.transform.position = new Vector2(-22.4909f, 32.04959f);
                }
                else if (mazmorra03vuelta)
                {
                    link.transform.position = new Vector2(-20.08911f, 34.5996f);
                }
                else if (mazmorra04vuelta)
                {
                    link.transform.position = new Vector2(-9.153177f, 34.67232f);
                }
                else if (mazmorra041vuelta)
                {
                    link.transform.position = new Vector2(8.907534f, 34.88992f);
                }
                else if (mazmorra05vuelta)
                {
                    link.transform.position = new Vector2(17.84668f, 34.71717f);
                }
                else if (mazmorra06vuelta)
                {
                    link.transform.position = new Vector2(22.88f, 4.42f);
                }
                break;
            case "Mazmorra1":
                if (mazmorra1vuelta)
                {
                    link.transform.position = new Vector2(-0.32f, 6.6f);
                }
                else
                {
                    link.transform.position = new Vector2(8.6f, 0.76f);
                }
                break;
            case "Mazmorra2":
                if (mazmorra2vuelta)
                {
                    link.transform.position = new Vector2(-0.02f, 6.55f);
                }
                else
                {
                    link.transform.position = new Vector2(8.5f, 0.46f);
                }
                break;
            case "Mazmorra4":
                if (mazmorra41)
                {
                    link.transform.position = new Vector2(8.88f, -5.92f);
                }
                else
                {
                    link.transform.position = new Vector2(-9.07f, -5.8f);
                }
                break;
            case "Mazmorra6":
                if (mazmorra6vuelta)
                {
                    link.transform.position = new Vector2(0.01f, 6.66f);
                }
                else
                {
                    link.transform.position = new Vector2(-8.394996f, 0.4650035f);
                }
                break;
            case "Mazmorra61":
                if (mazmorra61vuelta)
                {
                    link.transform.position = new Vector2(-0.06f, 16.52f);
                }
                else
                {
                    link.transform.position = new Vector2(-0.09f, -15.84f);
                }
                break;


            default:

                break;
        }
     

    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerPrefs.SetFloat("VidaLink", vida);
        //velActual = this.GetComponent<Rigidbody2D>().velocity.x;
        //velYActual = this.GetComponent<Rigidbody2D>().velocity.y;
        // Debug.Log("vel:" + velActual);
        //Debug.Log("vY:" + velYActual);
        //this.GetComponent<Animator>().SetFloat("vel", velActual);
        //this.GetComponent<Animator>().SetFloat("vely", velYActual);
        this.GetComponent<Animator>().SetBool("Arriba", arriba);
        this.GetComponent<Animator>().SetBool("Abajo", abajo);
        this.GetComponent<Animator>().SetBool("Derecha", derecha);
        this.GetComponent<Animator>().SetBool("Izquierda", izquierda);
        this.GetComponent<Animator>().SetBool("Espada", espada);
        if (this.vida <= 0)
        {
            Destroy(this.gameObject);
        }

        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
            arriba = true;
            abajo = false;
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            arriba = false;
            abajo = true;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            arriba = false;
            abajo = false;
}
        if (Input.GetKey("d"))
        {   
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = true;
            izquierda = false;
        }
        else if(Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = false;
            izquierda = true;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = false;
            izquierda = false;
        }
        if((arriba && derecha || arriba && izquierda || abajo && derecha || abajo && izquierda) && !diag)
        {
            vel = vel * 0.75f;
            velY = velY * 0.75f;
            diag = true;
        }
        else if (diag)
        {
            vel = 5f;
            velY = 5f;
            diag = false;
        }

        //Espadazo
        if (Input.GetKey("p"))
        {

            if (cooldown >= 15 && !espada)
            {
                espada = true;
                cooldown = 0;
            }
        }
        if (cooldown >= 15 && espada)
        {
            espada = false;
            cooldown = 0;
        }



    }
    private void FixedUpdate()
    {
        cooldown++;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Jarron")
        {
            if (espada == true)
            {
                collision.gameObject.GetComponent<JarronControler>().Objeto();
                Destroy(collision.gameObject);
            }

        }
        if (collision.gameObject.tag == "Enemigo")
        {
            if (espada == true)
            {
                Debug.Log(arriba);
                Debug.Log(derecha);
                Debug.Log(izquierda);
                Debug.Log(abajo);
                collision.gameObject.GetComponent<Enemigo>().KnockBack(this.arriba, this.abajo, this.izquierda, this.derecha);
                collision.gameObject.GetComponent<Enemigo>().vida -= this.ataque;
            }
        }
        if (onRupiasCambio != null)
        {
            
            rupias += onRupiasCambio();

        }


    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            KnockBack(collision.gameObject.GetComponent<Enemigo>().arriba, collision.gameObject.GetComponent<Enemigo>().abajo,
                collision.gameObject.GetComponent<Enemigo>().izquierda, collision.gameObject.GetComponent<Enemigo>().derecha);
            this.vida -= collision.gameObject.GetComponent<Enemigo>().ataque;

        }
        
    }
    public void KnockBack(bool arribaE, bool abajoE, bool izquierdaE, bool derechaE)
    {
        if (arribaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y + 1f);
        }
        else if (abajoE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y - 1f);
        }
        else if (izquierdaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x - 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        else if (derechaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x + 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        Invoke("EnableMovement", 0.7f);
        Color color = new Color(255 / 255f, 75 / 255f, 0 / 255f);
        this.GetComponent<SpriteRenderer>().color = color;

    }
    void EnableMovement()
    {
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
