﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bombas : MonoBehaviour
{
    
    public PlayerControler playerControler;
    public int bombas = 0;

    //Escribe en el HUD cuantas bombas tienes
    public void recuentoBombas()
    {

        bombas = PlayerControler.bombas;

        if (bombas >= 0 && bombas < 10)
        {
            this.GetComponent<Text>().text = "0" + bombas;
        }
        else this.GetComponent<Text>().text =""+ bombas;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Invoke para que no entre todo el rato
        Invoke("recuentoBombas", 0.7f);
    }
}
