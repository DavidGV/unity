﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flechas : MonoBehaviour
{

    public PlayerControler playerControler;
    public int flechas = 0;

    //Escribe en el HUD cuantas flechas tienes
    public void recuentoFlechas()
    {

        flechas = PlayerControler.flechas;

        if (flechas >= 0 && flechas < 10)
        {
            this.GetComponent<Text>().text = "0" + flechas;
        }
        else this.GetComponent<Text>().text = "" + flechas;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Invoke para que no entre todo el rato
        Invoke("recuentoFlechas", 0.7f);
    }
}
