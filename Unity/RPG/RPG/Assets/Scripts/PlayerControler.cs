﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour
{
    // Start is called before the first frame update
    //Velocidades
    public float vel;
    public float velY;
    private float velActual;
    private float velYActual;
    //Bools direcciones
    public bool arriba = false;
    public bool abajo = false;
    public bool derecha = false;
    public bool izquierda = false;
    private bool diag = true;
    //Sprint
    public bool sprint = false;
    //Cuando pegas
    private bool espada = false;
    //Cooldowns para volver a utilizar los diferentes ataques
    private int cooldown = 0;
    private int canShoot = 0;
    private int canPutBomb=0;
    //Anim
    private Animator anim;
    //Bools de la tienda que te permiten comprar
    private bool pocionTienda;
    private bool bombaTienda;
    private bool flechaTienda;
    //Bool menu
    public static bool menu=false;
    //GameObjects
    public GameObject menuIngame;
    public GameObject textoCasa;
    public GameObject textoTuto;
    public GameObject textoTutoFuera;
    public GameObject textoTienda;
    public GameObject textoTiendaFuera;
    //Valores Link
    public static float vida = 8f;
    public static float ataque = 1f;
    public static float xpNivel = 10f;
    public static float xpActual = 0;
    public static int nivel = 1;
    //Bools para cambio de escenas
    public static bool mapa0vuelta = false;
    public static bool mazmorra01vuelta = false;
    public static bool mazmorra02vuelta = false;
    public static bool mazmorra03vuelta = false;
    public static bool mazmorra04vuelta = false;
    public static bool mazmorra041vuelta = false;
    public static bool mazmorra05vuelta = false;
    public static bool mazmorra06vuelta = false;
    public static bool mazmorra1vuelta = false;
    public static bool mazmorra2vuelta = false;
    public static bool mazmorra41 = false;
    public static bool mazmorra6vuelta = false;
    public static bool mazmorra61vuelta = false;
    public static bool tiendaVuelta = false;
    public static bool tiendaVuelta1 = false;
    public static bool tiendaEntrada1 = false;
    public static bool tutorial = false;
    public static bool cueva = false;
    //Bools para activat y desactivar mensajes
    public static bool mensajeInicial = false;
    public static bool mensajeCambiarObjeto = false;
    public static bool tutorial1vez = false;
    public static bool mensajeTutorial = false;
    public static bool mensajeAcabadoTutorial = false;
    //GameObjects
    public GameObject[] flechasArray;
    public GameObject bomba;
    public GameObject newBomba;
    private GameObject newFlecha;
    //Consumibles
    public static int rupias = 0;
    public static int flechas = 0;
    public static int bombas = 0;
    public static int pociones = 0;

    //Singleton
    static GameObject link = null;

    //NombreMapa
    public string nombreMapa=null;

    //Booleano para pausar el juego al entrar en el menu ingame
    public static bool noMover=false;
    void Awake()
    {
        //Patró singleton
        if (link == null)
        {
            //crea el objecte en el primer moment
            link = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(link);
}
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
        //Actualiza el nombre del mapa
        nombreMapa = Application.loadedLevelName;
        //Dependiendo del nombre del mapa y los bools el player aparece en diferentes sitios, tambien activa mensajes dependiendo la escena
        switch (nombreMapa)
        {
            case "Mapa0":
                if (tutorial)
                {
                    if (!mensajeAcabadoTutorial)
                    {
                        textoTutoFuera.SetActive(true);
                        mensajeAcabadoTutorial = true;
                        Invoke("MensajeAcabadoTutorial", 7);
                        
                    }
                    link.transform.position = new Vector2(-13.4f, 1.42f);
                    
                }
                else if (tiendaVuelta)
                {
                    link.transform.position = new Vector2(-69.11f, 54.78f);
                }
                else if (tiendaVuelta1)
                {
                    link.transform.position = new Vector2(-65.21f, 54.78f);
                }
                else if (mapa0vuelta)
                {
                    
                    
                    link.transform.position = new Vector2(-5.02f, 69.12f);
                }
                else if (cueva)
                {
                    link.transform.position = new Vector2(16.77f, 68.65f);
                }
                else
                {
                    if (!mensajeCambiarObjeto)
                    {
                        textoTiendaFuera.SetActive(true);
                        mensajeCambiarObjeto = true;
                        Invoke("MensajeCambiarObjeto", 7);
                    }
                    link.transform.position = new Vector2(3.49f, -2.23f);
                }
                break;
            case "Mazmorra0":
                if (mazmorra01vuelta)
                {
                    link.transform.position = new Vector2(-24.45502f, 4.15f);
                }
                else if (mazmorra02vuelta)
                {
                    link.transform.position = new Vector2(-22.4909f, 32.04959f);
                }
                else if (mazmorra03vuelta)
                {
                    link.transform.position = new Vector2(-20.08911f, 34.5996f);
                }
                else if (mazmorra04vuelta)
                {
                    link.transform.position = new Vector2(-9.153177f, 34.67232f);
                }
                else if (mazmorra041vuelta)
                {
                    link.transform.position = new Vector2(8.907534f, 34.88992f);
                }
                else if (mazmorra05vuelta)
                {
                    link.transform.position = new Vector2(17.84668f, 34.71717f);
                }
                else if (mazmorra06vuelta)
                {
                    link.transform.position = new Vector2(22.88f, 4.42f);
                }
                break;
            case "Mazmorra1":
                if (mazmorra1vuelta)
                {
                    link.transform.position = new Vector2(-0.32f, 6.6f);
                }
                else
                {
                    link.transform.position = new Vector2(8.6f, 0.76f);
                }
                break;
            case "Mazmorra2":
                if (mazmorra2vuelta)
                {
                    link.transform.position = new Vector2(-0.02f, 6.55f);
                }
                else
                {
                    link.transform.position = new Vector2(8.5f, 0.46f);
                }
                break;
            case "Mazmorra4":
                if (mazmorra41)
                {
                    link.transform.position = new Vector2(8.88f, -5.92f);
                }
                else
                {
                    link.transform.position = new Vector2(-9.07f, -5.8f);
                }
                break;
            case "Mazmorra6":
                if (mazmorra6vuelta)
                {
                    link.transform.position = new Vector2(0.01f, 6.66f);
                }
                else
                {
                    link.transform.position = new Vector2(-8.394996f, 0.4650035f);
                }
                break;
            case "Mazmorra61":
                if (mazmorra61vuelta)
                {
                    link.transform.position = new Vector2(-0.06f, 16.52f);
                }
                else
                {
                    link.transform.position = new Vector2(-0.09f, -15.84f);
                }
                break;
            case "Tienda":
                if (tiendaEntrada1)
                {
                    link.transform.position = new Vector2(5.19f, -2.88f);
                }
                else
                {
                    link.transform.position = new Vector2(-4.71f, -2.88f);
                }

                textoTienda.SetActive(true);
                Invoke("MensajeTienda", 7);

                break;
            default:

                break;
        }
        if (!mensajeInicial)
        {
            textoCasa.SetActive(true);
            mensajeInicial = true;
            Invoke("MensajeInicial", 7);
        }
        if (!mensajeTutorial && PlayerControler.tutorial1vez )
        {
            textoTuto.SetActive(true);
            mensajeTutorial = true;
            Invoke("MensajeTutorial", 7);
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame

    void Update()
    {
        //Si el player no esta en el menu
        if (!noMover)
        {
            //PlayerPrefs
            PlayerPrefs.SetFloat("VidaLink", vida);
            PlayerPrefs.SetFloat("XpNivel", xpNivel);
            PlayerPrefs.SetFloat("XpActual", xpActual);
            //Bools animator
            this.GetComponent<Animator>().SetBool("Arriba", arriba);
            this.GetComponent<Animator>().SetBool("Abajo", abajo);
            this.GetComponent<Animator>().SetBool("Derecha", derecha);
            this.GetComponent<Animator>().SetBool("Izquierda", izquierda);
            this.GetComponent<Animator>().SetBool("Espada", espada);
            //Cuando pulsas "space" dependiendo el objetoActual llama a sus funciones
            if (Input.GetKeyDown("space"))
            {
                if (ObjetoActual.num == 2)
                {
                    Disparo();
                }
                if (ObjetoActual.num == 1)
                {
                    colocarBomba();
                }
                if (ObjetoActual.num == 0)
                {
                    if (pociones > 0)
                    {
                        TomarPocion();
                    }

                }


            }
            //Cuando el player muere resetea variables y te manda a la escena "MenuPrincipal"
            if (vida <= 0)
            {
                vida = 8f;
                rupias = 0;
                flechas = 0;
                bombas = 0;
                pociones = 0;
                ataque = 1f;
                xpNivel = 10f;
                xpActual = 0;
                nivel = 1;
                mensajeInicial = false;
                mensajeCambiarObjeto = false;
                tutorial1vez = false;
                mensajeTutorial = false;
                mensajeAcabadoTutorial = false;
                SceneManager.LoadScene("MenuPrincipal");

            }
            //Ir hacia arriba
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                arriba = true;
                abajo = false;
            }//Ir hacia abajo
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                arriba = false;
                abajo = true;
            }//Quedarse quieto en el eje y
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                arriba = false;
                abajo = false;
            }//Ir hacia la derecha    
            if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                derecha = true;
                izquierda = false;
            }//Ir hacia la izquierda
            else if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                derecha = false;
                izquierda = true;
            }//Quedarse quieto en el eje x
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                derecha = false;
                izquierda = false;
            }//Activas el sprint en el caso de que puedas y aumenta la velocidad
            if (Input.GetKey("left shift") && !BarraStamina.cero)
            {
                sprint = true;
                BarraStamina.gastar = true;
                vel = 10f;
                velY = 10f;
            }//Si no haces sprint vuelve a la velociad por defecto
            else
            {
                sprint = false;
                BarraStamina.gastar = false;
                vel = 5f;
                velY = 5f;
            }//Velocidad del sprint varia dependiendo de si vas recto o en diagonal, en diagonal hay que reducir la velocidad para que sea similar a la vel en recta
            if (sprint)
            {
                if ((arriba && derecha || arriba && izquierda || abajo && derecha || abajo && izquierda) && diag)
                {
                    vel = vel * 0.75f;
                    velY = velY * 0.75f;
                    diag = false;
                }
                else if (!diag)
                {
                    vel = 10f;
                    velY = 10f;
                    diag = true;
                }
            }//Velocidad normal varia dependiendo de si vas recto o en diagonal, en diagonal hay que reducir la velocidad para que sea similar a la vel en recta
            else
            {
                if ((arriba && derecha || arriba && izquierda || abajo && derecha || abajo && izquierda) && diag)
                {
                    vel = vel * 0.75f;
                    velY = velY * 0.75f;
                    diag = false;
                }
                else if (!diag)
                {
                    vel = 5f;
                    velY = 5f;
                    diag = true;
                }
            }
            //Ataque con espada
            if (Input.GetKey("p"))
            {

                if (cooldown == 15 && !espada)
                {
                    espada = true;
                    cooldown = 0;
                }
            }
            //Recuperar para poder hacer el ataque espada otra vez
            if (cooldown == 15 && espada)
            {
                espada = false;
                cooldown = 0;
            }
            //Activar menu y parar el juego
            if (Input.GetKey(KeyCode.Escape))
            {
                menu = true;
                menuIngame.SetActive(true);
                noMover = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

            }
            //Comprar en la tienda pociones en el caso de que estes en la poscion correcta, tengas rupias y pulses la tecla "b"
            if (pocionTienda && Input.GetKeyDown("b"))
            {
                if (rupias >= 20)
                {
                    rupias -= 20;
                    pociones++;
                }

            }//Comprar en la tienda bombas en el caso de que estes en la poscion correcta, tengas rupias y pulses la tecla "b"
            else if (bombaTienda && Input.GetKeyDown("b"))
            {
                if (rupias >= 10)
                {
                    rupias -= 10;
                    bombas++;
                }
            }//Comprar en la tienda flechas en el caso de que estes en la poscion correcta, tengas rupias y pulses la tecla "b"
            else if (flechaTienda && Input.GetKeyDown("b"))
            {
                if (rupias >= 5)
                {
                    rupias -= 5;
                    flechas++;
                }

            }

        }
    }
    //Le suma la vida al player y resta una pocion
    private void TomarPocion()
    {
        if(vida<8){
            PlayerControler.vida += 2;
            pociones--;
            if(vida>8){
                vida=8;
            }
        }
        
    }
    //En el fixedUpdate ponemos los Cooldowns para que siempre tengan el mismo tiempo
    private void FixedUpdate()
    {
        if (cooldown<15)
        {
            cooldown++;
        }
        if (canShoot < 25)
        {
            canShoot++;
        }
        if (canPutBomb < 15)
        {
            canPutBomb++;
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {   
        //Cuando el player sale del trigger donde se compra la pocion se pone el bool en false
        if (collision.gameObject.name == "comprarPocion")
        {
            pocionTienda = false;
        }//Cuando el player sale del trigger donde se compra la bomba se pone el bool en false
        else if (collision.gameObject.name == "comprarBomba")
        {
            bombaTienda = false;
        }//Cuando el player sale del trigger donde se compra la flecha se pone el bool en false
        else if (collision.gameObject.name == "comprarFlecha")
        {
            flechaTienda = false;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {   
        //Si colisiona el player con un jarron con la espada llama a la funcion del jarron que crea un objeto y luego lo destruye
        if (collision.gameObject.tag == "Jarron")
        {
            if (espada == true)
            {
                collision.gameObject.GetComponent<JarronControler>().Objeto();
                Destroy(collision.gameObject);
            }

        }//Si colisiona con un enmigo con la espada llama a la funcion KnockBack del enemigo y a la vida del enemigo le resta el ataque del player
        if (collision.gameObject.tag == "Enemigo")
        {
            if (espada == true)
            {
                collision.gameObject.GetComponent<Enemigo>().KnockBack(this.arriba, this.abajo, this.izquierda, this.derecha);
                collision.gameObject.GetComponent<Enemigo>().vida -= ataque;
            }
        }//Cuando el player entra en el trigger donde se compra la pocion se pone el bool en true
        if (collision.gameObject.name == "comprarPocion")
        {
            pocionTienda = true;
        }//Cuando el player entra en el trigger donde se compra la bomba se pone el bool en true
        else if (collision.gameObject.name == "comprarBomba")
        {
            bombaTienda = true;
        }//Cuando el player entra en el trigger donde se compra la flecha se pone el bool en true
        else if (collision.gameObject.name == "comprarFlecha")
        {
           flechaTienda = true;
        }//Cuando el player entra en el trigger de la rupia verde suma 1 al contador rupias
        if (collision.CompareTag("Verde"))
        {
           rupias++;
        }//Cuando el player entra en el trigger de la rupia azul suma 5 al contador rupias
        else if (collision.CompareTag("Azul"))
        {
           rupias+= 5;
        }//Cuando el player entra en el trigger de la rupia roja suma 20 al contador rupias
        else if (collision.CompareTag("Roja"))
        {
            rupias += 20;
        }//Si el contador de rupias es mas grande que 999 es sera el valor que tiendra
        if (rupias > 999)
        {
            rupias = 999;
        }
        //Cuando el player entra en el trigger de la flecha suma una flecha al contador de las flechas, valor maximo 30
        if (collision.CompareTag("Flecha"))
        {
            flechas++;
            if (flechas > 30)
            {
                flechas = 30;
            }

        }//Cuando el player entra en el trigger de la bomba suma una bomba al contador de las bombas, valor maximo 10
        else if (collision.CompareTag("Bomba"))
        {
            bombas++;
            if (bombas > 10)
            {
                bombas = 10;
            }

        }//Cuando el player entra en el trigger del corazon suma 1 a la vida
        else if(collision.CompareTag("Corazon"))
        {
            if(vida<8){
                vida += 1f;
            }
            
        }
    }
    //Recibe los bools del enemigo con el que ha colisionado y hace el knockback en la direccion que le toque
    //Tambien le cambia el color y hace un Invoke a EnableMovement() para que vuelva a su estado normal
    public void KnockBack(bool arribaE, bool abajoE, bool izquierdaE, bool derechaE)
    {
        if (arribaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y + 1f);
        }
        else if (abajoE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y - 1f);
        }
        else if (izquierdaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x - 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        else if (derechaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x + 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        Invoke("EnableMovement", 0.7f);
        Color color = new Color(255 / 255f, 75 / 255f, 0 / 255f);
        this.GetComponent<SpriteRenderer>().color = color;

    }//Vuelve el color por defecto del player
    void EnableMovement()
    {
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }
    //Disparo comprueba la animacion del player y dependiendo de la animacion dispara la flecha en la direccion que toque y resta una flecha
    public void Disparo()
    {
        if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkArriba") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleArriba")))
        {
            newFlecha = Instantiate(flechasArray[0]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 20);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1f);
            canShoot = 0;
            PlayerControler.flechas--;
        }
        else if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkAbajo") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleAbajo")))
        {
            newFlecha = Instantiate(flechasArray[1]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -20);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - 1f);
            canShoot = 0;
            PlayerControler.flechas--;
        }
        else if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkDerecha") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleDerecha")))
        {
            newFlecha = Instantiate(flechasArray[2]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(+20, 0);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x + 1f, this.transform.position.y);
            canShoot = 0;
            PlayerControler.flechas--;
        }
        else if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkIzquierda") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleIzquierda")))
        {
            newFlecha = Instantiate(flechasArray[3]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(-20, 0);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x - 1f, this.transform.position.y);
            canShoot = 0;
            PlayerControler.flechas--;
        }
        
    }
    //Instancia una bomba en la posicion del personaje y resta una bomba
    public void colocarBomba()
    {
        if (bombas > 0 && canPutBomb == 15)
        {
            newBomba = Instantiate(bomba);
            newBomba.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
            canPutBomb = 0;
            bombas--;
        }
    }
    //Desactivan los textos las siguientes 5 funciones
    public void MensajeInicial()
    {
        textoCasa.SetActive(false);
    }
    public void MensajeCambiarObjeto()
    {
        textoTiendaFuera.SetActive(false);
    }
    public void MensajeTutorial()
    {
        textoTuto.SetActive(false);
    }
    public void MensajeAcabadoTutorial()
    {
        textoTutoFuera.SetActive(false);
    }
    public void MensajeTienda()
    {
        textoTienda.SetActive(false);
    }
    
}
