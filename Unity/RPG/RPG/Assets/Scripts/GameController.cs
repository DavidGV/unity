﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    //camara del juego
    public Camera camera;
    public Dictionary<string, int> playerInfo;
    bool exit = false;

    //nuestro personaje principal
    public PlayerControler player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //seguimiento de la camara al personaje
        camera.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            player.transform.position.z - 100);


    }
}
