﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [Range (0f, 0.20f)]
    public float parallaxSpeed = 0.02f;
    public RawImage background1;
    public RawImage background2;

    public enum GameState {Idle, PLaying};
    public GameState gameState = GameState.Idle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Empieza el juego
        if (gameState == GameState.Idle && (Input.GetKeyDown("w") || Input.GetMouseButtonDown(0))){
            gameState = GameState.PLaying;
        }
        else if (gameState == GameState.PLaying)
        {
            Parallax();
            
        }
        
    }
    void Parallax()
    {
       /* if (num == 1)
        {*/
            float finalSpeed = parallaxSpeed * Time.deltaTime;
            background1.uvRect = new Rect(background1.uvRect.x + finalSpeed, 0f, 1f, 1f);
            background2.uvRect = new Rect(background2.uvRect.x + finalSpeed * 2, 0f, 1f, 1f);
       // }
        /*else if (num == -1)
        {
            float finalSpeed = parallaxSpeed * Time.deltaTime;
            background1.uvRect = new Rect(background1.uvRect.x - finalSpeed, 0f, 1f, 1f);
            background2.uvRect = new Rect(background2.uvRect.x - finalSpeed * 2, 0f, 1f, 1f);
        }*/

    }
}
