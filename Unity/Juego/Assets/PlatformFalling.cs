﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFalling : MonoBehaviour
{
    public float fallDelay = 1f;
    public float respawnDelay = 5f;
    public bool playerTouch;
    public GameObject platformMovilL;
    public GameObject platformMovilR;
    public GameObject platformMovilD;

    private Rigidbody2D rb2d;
    private PolygonCollider2D pc2d;
    private Vector3 start;
    private Animator pLanim;
    private Animator pRanim;
    private Animator pDanim;
    // public Animator anim;


    // Start is called before the first frame update
    void Start()
    {
       // anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        pc2d = GetComponent<PolygonCollider2D>();

        pLanim = platformMovilL.GetComponent<Animator>();
        pRanim = platformMovilR.GetComponent<Animator>();
        pDanim = platformMovilD.GetComponent<Animator>();
        //PlatformMovilL = GetComponent<>();
        /*PlatformMovilR = GetComponent<GameObject>();
        PlatformMovilD = GetComponent<GameObject>();*/

        start = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        pLanim.SetBool("PlayerTouch", playerTouch);
        pRanim.SetBool("PlayerTouch", playerTouch);
        pDanim.SetBool("PlayerTouch", playerTouch);

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //playerTouch = true;
            Invoke("Fall", fallDelay);
            Invoke("Respawn", fallDelay + respawnDelay);
            //playerTouch = true;

        }

    }
    void Fall()
    {
        rb2d.isKinematic = false;
        pc2d.isTrigger = true;
    }
    void Respawn()
    {
        transform.position = start;
        rb2d.isKinematic = true;
        pc2d.isTrigger = false;
        //playerTouch = false;
        rb2d.velocity = Vector3.zero;

    }
    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
          
        }
    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Sale");
            playerTouch = false;

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("Entra");
            playerTouch = true;

        }
    }

}
