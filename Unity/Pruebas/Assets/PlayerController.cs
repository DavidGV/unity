﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float fJump;


    private bool puedeSaltar = true;
    private int vidas = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (vidas <= 0)
        {
            Destroy(this.gameObject);
        }
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y));
            // this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y);
            // Debug.Log("speed:"+ this.GetComponent<Rigidbody2D>().velocity);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-speed, this.GetComponent<Rigidbody2D>().velocity.y));
            //this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
            // Debug.Log("speed:" + this.GetComponent<Rigidbody2D>().velocity);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, this.GetComponent<Rigidbody2D>().velocity.y));
            //this.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (Input.GetKey("w") && puedeSaltar)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, fJump));
            puedeSaltar = false;
        }
        
        
       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            puedeSaltar = true;
           // Debug.Log("nombre:" + collision.gameObject.name);
        } else if (collision.gameObject.tag == "Enemigo")
        {

            if (this.GetComponent<Rigidbody2D>().velocity.x > 0.1)
            {
                vidas--;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed * 5, this.GetComponent<Rigidbody2D>().velocity.y);
                Debug.Log("vidas:" + vidas);
            }
            
            else if (this.GetComponent<Rigidbody2D>().velocity.x < 0.1)
            {
                vidas--;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(speed * 1000f, this.GetComponent<Rigidbody2D>().velocity.y));
                Debug.Log("vidas:" + vidas);
            }
        }
    }
}
