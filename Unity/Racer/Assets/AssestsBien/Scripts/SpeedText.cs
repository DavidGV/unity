﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpeedText : MonoBehaviour
{
    private Text speedText;
    [SerializeField]
    private SpeedSO speed;


    void Start()
    {
        speedText = this.GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        //Updatea la vel
        speedText.text = speed.Speed+"";
    }
}
