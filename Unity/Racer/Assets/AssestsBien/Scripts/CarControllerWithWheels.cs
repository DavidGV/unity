﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarControllerWithWheels : MonoBehaviour
{
    public List<AxleInfo> axleInfos;
    public static float maxMotorTorque = 0f;
    public float maxSteeringAngle;
    public GameObject mainCamera;
    public GameObject fpCamera;
    public GameObject textRev;
    public GameObject humo;
    public GameObject frenoIzq;
    public GameObject frenoDer;
    public GameObject izqAtras;
    public GameObject derAtras;
    private WheelFrictionCurve wfcLeft;
    private WheelFrictionCurve wfcRight;
    private float motor;
    //public static double speed;
    [SerializeField]
    private SpeedSO speed;
    private bool calar = false;
    private bool fPerson = false;
    private float maxVel = 30f;

    public void Start()
    {
        //Subscripcion al cambio de marchas
        this.GetComponent<Marchas>().eventcambioMarcha += CambioDeMarchas;
        maxMotorTorque = 0f;
        speed.Speed = this.gameObject.GetComponent<Rigidbody>().velocity.x * this.gameObject.transform.forward.x + this.gameObject.GetComponent<Rigidbody>().velocity.z * this.gameObject.transform.forward.z;
        wfcLeft = izqAtras.GetComponent<WheelCollider>().sidewaysFriction;
        wfcRight = derAtras.GetComponent<WheelCollider>().sidewaysFriction;
    }
    //Movimiento de las ruedas visualmente
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.rotation = rotation;
    }
    public void Update()
    {
        //Voltea el coche por si vuelca
        if (Input.GetKeyDown("f"))
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);
            this.transform.Rotate(new Vector3(0, 0, 180));

        }
        //Cambia entre 1ª y 3ª persona
        if (Input.GetKeyDown("c"))
        {
            if (!fPerson)
            {
                mainCamera.SetActive(false);
                fpCamera.SetActive(true);
                fPerson = true;
            }
            else
            {
                mainCamera.SetActive(true);
                fpCamera.SetActive(false);
                fPerson = false;
            }
        }
        //Derrapar
        if (Input.GetKey("space"))
        {
            wfcLeft.stiffness = 3;
            wfcRight.stiffness = 3;
            izqAtras.GetComponent<WheelCollider>().sidewaysFriction = wfcLeft;
            derAtras.GetComponent<WheelCollider>().sidewaysFriction = wfcRight;
        }
        else
        {
            wfcLeft.stiffness = 5;
            wfcRight.stiffness = 5;
            izqAtras.GetComponent<WheelCollider>().sidewaysFriction = wfcLeft;
            derAtras.GetComponent<WheelCollider>().sidewaysFriction = wfcRight;
        }
        //Calcula la velocidad
        speed.Speed = this.gameObject.GetComponent<Rigidbody>().velocity.x * this.gameObject.transform.forward.x + this.gameObject.GetComponent<Rigidbody>().velocity.z * this.gameObject.transform.forward.z;
        speed.Speed = (speed.Speed * 3600) / 1000;
        speed.Speed = Mathf.Round(speed.Speed);
        ComprobarVel();
        //Si se cala el coche motor = 0
        if (calar)
        {
            motor = 0;
        }
    }
    public void FixedUpdate()
    {
        //Movimiento hacia delante
        if (Input.GetAxis("Vertical") > 0 && !calar)
        {
            if (speed.Speed > maxVel)
            {
                motor = 0;
            }
            else
            {
                motor = maxMotorTorque * Input.GetAxis("Vertical");
            }
            frenoIzq.SetActive(false);
            frenoDer.SetActive(false);
        }
        //Movimiento hacia atras
        else if (Input.GetAxis("Vertical") < 0)
        {
            frenoIzq.SetActive(true);
            frenoDer.SetActive(true);
            if (speed.Speed < -20f)
            {
                motor = 0;
            }
            else
            {
                motor = 0;
                if (Marchas.marcha == 0)
                {
                    motor = 1000f * Input.GetAxis("Vertical");
                }
                else
                {
                    motor = maxMotorTorque * Input.GetAxis("Vertical");
                }
            }


        }
        //Cuando no pulsas nada
        else
        {
            frenoIzq.SetActive(false);
            frenoDer.SetActive(false);
        }
        //Girar
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
    }
    //CambioDeMarchas cambia el maxMotorTorque y la maxVel, mientras la marcha es mas baja tiene
    //mas fuerza pero menos velocidad
    private void CambioDeMarchas()
    {
        switch (Marchas.marcha)
        {
            case 0:
                maxMotorTorque = 0f;
                maxVel = 20f;
                break;
            case 1:
                maxMotorTorque = 1000f;
                maxVel = 20f;
                break;
            case 2:
                maxMotorTorque = 900f;
                maxVel = 35f;
                break;
            case 3:
                maxMotorTorque = 800f;
                maxVel = 50f;
                break;
            case 4:
                maxMotorTorque = 700f;
                maxVel = 70f;
                break;
            case 5:
                maxMotorTorque = 600f;
                maxVel = 100f;
                break;
            case 6:
                maxMotorTorque = 500f;
                maxVel = 200f;
                break;
            default:
                break;
        }
    }
    //ComprobarVel mira a la vel que va el coche, si es inferior a la minima dependiendo
    //de la marcha llama a una corrutina, que lo que hace es mirar si en 3s sigues en 
    //la misma marcha con una vel reducida, si la respuesta es afirmativa, el coche se cala
    //al calarse el coche no acelera mas y tienes que frenar hasta estar con una vel por 
    //debajo de los 10km/h y entonces ya puedes volver a conducir con normalidad
    private void ComprobarVel()
    {
        switch (Marchas.marcha)
        {
            case 0:
                if (speed.Speed < 10)
                {
                    calar = false;
                    textRev.GetComponent<Text>().color = Color.white;
                    humo.SetActive(false);
                }
                break;
            case 1:
                if (speed.Speed>20)
                {
                   
                }
                break;
            case 2:
                if (speed.Speed < 10)
                {
                    StartCoroutine(CorrutinaCalar(3f, Marchas.marcha, 10));
                }
                break;
            case 3:
                if (speed.Speed < 20)
                {
                    StartCoroutine(CorrutinaCalar(3f, Marchas.marcha, 20));
                }
                break;
            case 4:
                if (speed.Speed < 40)
                {
                    StartCoroutine(CorrutinaCalar(3f, Marchas.marcha, 40));
                }
                break;
            case 5:
                if (speed.Speed < 60)
                {
                    StartCoroutine(CorrutinaCalar(3f, Marchas.marcha, 60));
                }
                break;
            case 6:
                if (speed.Speed < 80)
                {
                    StartCoroutine(CorrutinaCalar(3f, Marchas.marcha, 800));
                }
                break;
            default:
                break;
        }

    }
    //Corrutina explicada arriba
    IEnumerator CorrutinaCalar(float f, int marcha, float vel)
    {
        yield return new WaitForSeconds(f);
        if (Marchas.marcha == marcha && speed.Speed<vel)
        {
            print("CALAO");
            if (!calar)
            {
                calar = true;
                Marchas.marcha = 0;
                CambioDeMarchas();
                humo.SetActive(true);
                textRev.GetComponent<Text>().text = "0";
                textRev.GetComponent<Text>().color = Color.red;
            }
        }
    }
    //Si colisiona con un checkpoint suma 1 al cont
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "CheckPoint")
        {
            CheckPointRace.count++;
            print("Checkpoint: " + CheckPointRace.count);
        }
    }
    [System.Serializable]
    public class AxleInfo
    {
        public WheelCollider leftWheel;
        public WheelCollider rightWheel;
        public bool motor;
        public bool steering; 
    }
}

