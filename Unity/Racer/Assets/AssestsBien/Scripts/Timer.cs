﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Timer : MonoBehaviour
{
    public Text timerText;
    private float starTime;
    public static bool start = false;
    [SerializeField]
    private TiempoSO bestTime;
    float t = 0;
    void Start()
    {
        bestTime.BestTime = 99.9999f;
    }

    void Update()
    {
        //Si el coche toca la meta empieza el contador
        //El tiempo se pasa a min, seg, milisegs y los muestra
        if (start)
        {
            t = Time.time - starTime;
            string mins = ((int)t / 60).ToString("00");
            string segs = (t % 60).ToString("00");
            string milisegs = ((t * 100) % 100).ToString("00");
            string mins2 = ((int)bestTime.BestTime / 60).ToString("00");
            string segs2 = (bestTime.BestTime % 60).ToString("00");
            string milisegs2 = ((bestTime.BestTime * 100) % 100).ToString("00");
            timerText.text = "Time: " + string.Format("{00}:{01}:{02}", mins, segs, milisegs) + "\nBest time: "+ string.Format("{00}:{01}:{02}", mins2, segs2, milisegs2);
        }
    }
    //Si colisionas con la meta
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "FinalPoint")
        {
            //La primera vez hace que comienze el contador del tiempo
            if (!start)
            {
                start = true;
                starTime = Time.time;
            }
            //Las siguientes veces si el contador de checkPoints es 10
            //Comprueba si el tiempo es mejor al actual y lo actualiza
            //Resetea el tiempo
            else
            {
                if(CheckPointRace.count >= 10)
                {
                    if(t < bestTime.BestTime)
                    {
                        bestTime.BestTime = t;
                    }
                    starTime = Time.time;
                    CheckPointRace.count = 0;
                }
            }
            
        }
    }

}
