﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Speed", menuName = "Speed Data", order = 25)]
public class SpeedSO : ScriptableObject
{
    [SerializeField]
    private float speed;
    public float Speed { get; set; }
}
