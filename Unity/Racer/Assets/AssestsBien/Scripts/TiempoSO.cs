﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BestTime", menuName = "BestTime Data", order = 26)]
public class TiempoSO : ScriptableObject
{
    [SerializeField]
    private float bestTime;
    public float BestTime { get; set; }

}
