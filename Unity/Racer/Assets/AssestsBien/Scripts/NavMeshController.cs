﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshController : MonoBehaviour
{
    //Intento de navMesh
    private NavMeshAgent agent;
    public Transform[] targets;
    private int count = 0;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Timer.start)
        {
            agent.destination = targets[0].position;
        }
    }
}
