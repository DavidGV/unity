﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject car;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mainCamera.transform.position = new Vector3
		(
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
			Mathf.Lerp(mainCamera.transform.position.x, (car.transform.position.x - (car.transform.forward.x * 10)), Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.y, (car.transform.position.y - (car.transform.forward.y * 10))+5, Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.z, (car.transform.position.z - (car.transform.forward.z * 10)), Time.deltaTime * 10)
		);
        mainCamera.transform.LookAt(car.transform);
    }
}
