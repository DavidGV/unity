﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextoCanvas : MonoBehaviour
{
    // Start is called before the first frame update
    private Text marchasTexto;
    public GameObject coche;
    void Start()
    {
        //Se suscribe al evento de cambiar marchas
        coche.GetComponent<Marchas>().eventcambioMarcha += CambioDeMarchas2;
        marchasTexto = this.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    //Cambia el numero de las marchas
    private void CambioDeMarchas2()
    {
        marchasTexto.text = Marchas.marcha.ToString();
    }
}
