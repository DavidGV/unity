﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Revoluciones2 : MonoBehaviour
{
    public Transform nivel;
    [SerializeField]
    private SpeedSO speed;
    public float valorActual = 0;
    public float valorInicial = 20;
    public float valorMinimo = 0;
    public GameObject car;

    void Start()
    {
        //Suscripcion al cambio de marchas
        car.GetComponent<Marchas>().eventcambioMarcha += CambioDeMarchas;
    }

    void Update()
    {
        //Updatea el circulo de las revoluciones segun la marcha
        nivel.GetComponent<Image>().fillAmount = (((speed.Speed- valorMinimo) * 100f) / (valorInicial - valorMinimo)) * 0.01f;
    }
    //Cambio de valores maximos y minimos del circulo de las revoluciones dependiendo la marcha
    private void CambioDeMarchas()
    {
        switch (Marchas.marcha)
        {
            case 0:
                valorInicial = 20;
                valorMinimo = 0;
                break;
            case 1:
                valorInicial = 20;
                valorMinimo = 0;
                break;
            case 2:
                valorInicial = 35;
                valorMinimo = 10;
                break;
            case 3:
                valorInicial = 50;
                valorMinimo = 20;
                break;
            case 4:
                valorInicial = 70;
                valorMinimo =40;
                break;
            case 5:
                valorInicial = 100;
                valorMinimo = 60;
                break;
            case 6:
                valorInicial = 200;
                valorMinimo = 80;
                break;
            default:
                break;
        }
    }
}
