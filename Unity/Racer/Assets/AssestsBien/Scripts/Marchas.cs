﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marchas : MonoBehaviour
{
    public static int marcha = 0;
    public delegate void cambioMarcha();
    public event cambioMarcha eventcambioMarcha;
    [SerializeField]
    private SpeedSO speed;
    // Start is called before the first frame update
    void Start()
    {
        marcha = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Si pulsas e subes de marcha
        if (Input.GetKeyDown("e"))
        {
            switch(marcha)
            {
                case 0:
                    if (speed.Speed < 10f)
                    {
                        marcha = 1;
                        eventcambioMarcha();
                    } 
                    break;
                case 1:
                    marcha = 2;
                    eventcambioMarcha();
                    break;
                case 2:
                    marcha = 3;
                    eventcambioMarcha();
                    break;
                case 3:
                    marcha = 4;
                    eventcambioMarcha();
                    break;
                case 4:
                    marcha = 5;
                    eventcambioMarcha();
                    break;
                case 5:
                    marcha = 6;
                    eventcambioMarcha();
                    break;
                default:
                    break;
            }
        } 
        //si pulsas q bajas de marcha
        else if (Input.GetKeyDown("q"))
        {
            switch (marcha)
            {
                case 1:
                    marcha = 0;
                    eventcambioMarcha();
                    break;
                case 2:
                    marcha = 1;
                    eventcambioMarcha();
                    break;
                case 3:
                    marcha = 2;
                    eventcambioMarcha();
                    break;
                case 4:
                    marcha = 3;
                    eventcambioMarcha();
                    break;
                case 5:
                    marcha = 4;
                    eventcambioMarcha();
                    break;
                case 6:
                    marcha = 5;
                    eventcambioMarcha();
                    break;
                default:
                    break;
            }
        }
    }
}