﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {

    public GameObject palmera;
    public GameObject arbol1;
    public GameObject arbol2;
    public GameObject pino;
    public GameObject contenedor;
    //enum para decidir el tipo de cosa que vas a dibujar
    public enum DrawMode {NoiseMap, ColourMap, Mesh};
	//un enum en el inspector se ve como un selector
	public DrawMode drawMode;

	const int mapChunkSize = 100;
	[Range(0,6)]
	public int levelOfDetail;
	public float noiseScale;

	public int octaves;
	[Range(0,1)]
	public float persistance;
	public float lacunarity;

	public int seed;
    public int secondseed;
    public Vector2 offset;

	public float meshHeightMultiplier;
	public AnimationCurve meshHeightCurve;

	public bool autoUpdate;

	public TerrainType[] regions;

	public void GenerateMap() {
        int bv = contenedor.transform.childCount;
        for (int f = 0; f < bv; f++)
        {
            DestroyImmediate(contenedor.transform.GetChild(f).gameObject);
            print("asdasd");
        }
        float[,] noiseMap = Noise.GenerateNoiseMap (mapChunkSize, mapChunkSize, seed, secondseed, noiseScale, octaves, persistance, lacunarity, offset);
             
        Color[] colourMap = new Color[mapChunkSize * mapChunkSize];
		for (int y = 0; y < mapChunkSize; y++) {
			for (int x = 0; x < mapChunkSize; x++) {
				//por cada puntod el mapa miras la altura
				float currentHeight = noiseMap [x, y];
				//y recorres el struct de regiones (colores)
				for (int i = 0; i < regions.Length; i++) {
					//si la region actual es mayor que la altura
					if (currentHeight <= regions [i].height) {
						//la pintas de este color
						colourMap [y * mapChunkSize + x] = regions [i].colour;
               
                        if (i==3 && (currentHeight>=0.42 && currentHeight <= 0.48))
                        {
                            
                            int rand = (Random.Range(0, 1001));
                            //print("numero: " + rand);
                            if (rand>900)
                            {
                                //print("entro: " + rand);
                                //print("currentHeight: "+ currentHeight);
                                GameObject npalmera = Instantiate(palmera);
                                npalmera.transform.parent = contenedor.transform;
                                npalmera.transform.position = new Vector3(x, meshHeightCurve.Evaluate(currentHeight) * meshHeightMultiplier, (mapChunkSize - y)-0.5f);
                                //print('a');
                            }
                        }
                        if (i == 5 && (currentHeight >= 0.62 && currentHeight <= 0.68))
                        {
                            int rand = (Random.Range(0, 1001));
                            //print("numero: " + rand);
                            if (rand > 850)
                            {
                                //print("entro: " + rand);
                                int rand2 = (Random.Range(0, 2));
                                GameObject narbol;
                                if (rand2 == 0)
                                {
                                    narbol = Instantiate(arbol1);
                                }
                                else
                                {
                                    narbol = Instantiate(arbol2);
                                }
                                narbol.transform.parent = contenedor.transform;
                                narbol.transform.position = new Vector3(x, meshHeightCurve.Evaluate(currentHeight) * meshHeightMultiplier, (mapChunkSize - y) - 0.5f);
                                //print('a');
                            }
                        }
                        if (i == 7 && currentHeight >= 0.82)
                        {
                            int rand = (Random.Range(0, 1001));
                            //print("numero: " + rand);
                            if (rand > 800)
                            {
                                //print("entro: " + rand);
                                GameObject npino = Instantiate(pino);
                                npino.transform.parent = contenedor.transform;
                                npino.transform.position = new Vector3(x, meshHeightCurve.Evaluate(currentHeight) * meshHeightMultiplier, (mapChunkSize - y) - 0.5f);
                                //print('a');
                            }
                        }
                        //y haces break. obviamente esto solo funciona si las regiones estan ordenadas ascendientemente por altura
                        break;
					}
				}
			}
		}

		MapDisplay display = FindObjectOfType<MapDisplay> ();
		if (drawMode == DrawMode.NoiseMap) {
			display.DrawTexture (TextureGenerator.TextureFromHeightMap (noiseMap));
		} else if (drawMode == DrawMode.ColourMap) {
			display.DrawTexture (TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));
		} else if (drawMode == DrawMode.Mesh) {
			display.DrawMesh (MeshGenerator.GenerateTerrainMesh (noiseMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail), TextureGenerator.TextureFromColourMap (colourMap, mapChunkSize, mapChunkSize));
		}
	}

    //onValidate salta automaticamente cuando cambias los valores. Es de Unity. 
	void OnValidate() {
		if (lacunarity < 1) {
			lacunarity = 1;
		}
		if (octaves < 0) {
			octaves = 0;
		}
	}
}

[System.Serializable]
public struct TerrainType {
	public string name;
	public float height;
	public Color colour;
}