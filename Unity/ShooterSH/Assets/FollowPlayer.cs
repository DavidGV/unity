﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    public Transform inicio;
    private bool follow = false;
    private int inRange = 0;
    // Start is called before the first frame update
    void Start()
    {
       // inicio = this.gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (follow)
        {
            this.gameObject.GetComponent<NavMeshAgent>().SetDestination(player.position);
        }
        else
        {
            this.gameObject.GetComponent<NavMeshAgent>().SetDestination(inicio.position);
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Player")
        {
            player = other.transform;
            follow = true;
            inRange++;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            inRange--;
            if (inRange == 0)
            {
                follow = false;
            }
        }
    }
}
