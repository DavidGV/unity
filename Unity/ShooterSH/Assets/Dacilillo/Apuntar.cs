﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Apuntar : MonoBehaviour
{
    public Camera playerCamera;
    //public GameObject arma;
    public GameObject canvas;
    public GameObject mira;
    public GameObject scopeSniper;
    public static int armaActual = 1;
    bool apuntar = false;
    Animator anim;
    Animator animCamera;
    private void Awake()
    {
        canvas = GameObject.Find("CanvasFPS");
        mira = canvas.transform.GetChild(0).gameObject;
        scopeSniper = canvas.transform.GetChild(1).gameObject;
    }
    void Start()
    {
        //anim = this.GetComponent<Animator>();
        anim = this.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<Animator>();
        animCamera = this.transform.GetChild(0).gameObject.GetComponent<Animator>();
    }
    void Update()
    {
        if (this.transform.GetComponent<PhotonView>().IsMine)
        {
            if (Input.GetMouseButtonDown(1))
            {
                apuntar = true ? !apuntar : apuntar; 
                ApuntarDesapuntar();
            }
            if (Input.GetKeyDown("1"))
            {
                armaActual = 1;
                ChangeArma();
            } 
            else if (Input.GetKeyDown("2"))
            {
                armaActual = 2;
                ChangeArma();
            }
            else if (Input.GetKeyDown("3"))
            {
                armaActual = 3;
                ChangeArma();
            }
            else if (Input.GetKeyDown("4"))
            {
                armaActual = 4;
                ChangeArma();
            }
            else if (Input.GetKeyDown("5"))
            {
                armaActual = 5;
                ChangeArma();
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                if (armaActual > 1)
                {
                    armaActual--;
                }
                else if (armaActual == 1)
                {
                    armaActual = 5;
                }
                ChangeArma();

            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                if (armaActual < 5)
                {
                    armaActual++;
                }
                else if (armaActual == 5)
                {
                    armaActual = 1;
                }
                ChangeArma();
           
            }
        }
        else
        {
            this.transform.GetComponentInChildren<Camera>().enabled = false;
        }

    }
    /// <summary>
    /// Cuando el sniper apunta se llama a la funcion ScopeSniper() que es una corrutina y a los 0.15f activa la mira del sniper 
    /// </summary>
    /// <returns></returns>
    private IEnumerator ScopeSniper()
    {
        yield return new WaitForSeconds(0.15f);
        scopeSniper.SetActive(true);
        this.transform.GetChild(0).transform.GetChild(6).gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    /// <summary>
    /// ApuntarDesapuntar() comprueba si estamos apuntando o no y en funcion de eso activa o desactiva el booleano de apuntar
    /// que sirve para la animacion, en el caso de que el arma actual sea 4(Sniper) llamas a ScopeSniper()
    /// </summary>
    private void ApuntarDesapuntar()
    {
        if (apuntar)
        {
            mira.SetActive(false);
            if (armaActual == 4)
            {
                StartCoroutine(ScopeSniper());
            }
        }
        else
        {
            mira.SetActive(true);
            this.transform.GetChild(0).transform.GetChild(6).gameObject.GetComponent<MeshRenderer>().enabled = true;
            scopeSniper.SetActive(false);
        }
        anim.SetBool("Apuntar", apuntar);
        animCamera.SetBool("Apuntar", apuntar);
    }
    /// <summary>
    /// Cambia el int del arma actual para la animacion, desactiva todas las armas y luego en funcion del armaActual activa
    /// el arma que toca
    /// </summary>
    private void ChangeArma()
    {
        animCamera.SetInteger("ArmaActual", armaActual);
        apuntar = false;
        this.transform.GetChild(0).transform.GetChild(6).gameObject.GetComponent<MeshRenderer>().enabled = true;
        scopeSniper.SetActive(false);
        ApuntarDesapuntar();
        for (int i = 0; i < 10; i++)
        {
            this.transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(false);
        }
        switch (armaActual)
        {
            case 1:
                this.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
                anim = this.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<Animator>();
                break;
            case 2:
                this.transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(true);
                this.transform.GetChild(0).transform.GetChild(3).gameObject.SetActive(true);
                anim = this.transform.GetChild(0).transform.GetChild(2).gameObject.GetComponent<Animator>();
                break;
            case 3:
                this.transform.GetChild(0).transform.GetChild(4).gameObject.SetActive(true);
                this.transform.GetChild(0).transform.GetChild(5).gameObject.SetActive(true);
                anim = this.transform.GetChild(0).transform.GetChild(4).gameObject.GetComponent<Animator>();
                break;
            case 4:
                this.transform.GetChild(0).transform.GetChild(6).gameObject.SetActive(true);
                this.transform.GetChild(0).transform.GetChild(7).gameObject.SetActive(true);
                anim = this.transform.GetChild(0).transform.GetChild(6).gameObject.GetComponent<Animator>();
                break;
            case 5:
                this.transform.GetChild(0).transform.GetChild(8).gameObject.SetActive(true);
                this.transform.GetChild(0).transform.GetChild(9).gameObject.SetActive(true);
                anim = this.transform.GetChild(0).transform.GetChild(8).gameObject.GetComponent<Animator>();
                break;
            default:
                break;
        }

    }

}
