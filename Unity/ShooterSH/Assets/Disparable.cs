﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparable : MonoBehaviour
{
    public Personaje personaje;
    public GameObject ragdollZombie;
    public GameEvent eliminacion;
    private void Awake()
    {
        if (personaje.vida <= 0)
        {
            personaje.vida = 250;
        }
    }

    public void kill()
    {
        GameObject ragdoll = Instantiate(ragdollZombie, this.transform.position, this.transform.rotation);
        eliminacion.Raise();
        Destroy(this.gameObject);   
    }

}
