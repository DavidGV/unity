﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ScriptableObject de los personajes
/// </summary>
public abstract class Personaje : ScriptableObject
{

    public new string name;

    /// <summary>
    /// Vida del personaje
    /// </summary>
    public float vida;

    /// <summary>
    /// Velocidad del personaje
    /// </summary>
    public float velocidad;

    

}

