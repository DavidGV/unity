﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Zombie basico y tal
/// </summary>
 [CreateAssetMenu(fileName = "Zombie", menuName = "Zombie", order = 3)]
public class Zombie : Personaje
{
    /// <summary>
    /// Daño del señor/a zombi a Players
    /// </summary>
    public float ataque;

    /// <summary>
    /// velocidad de ataque
    /// </summary>
    public float velocidadAtaque;
    
}
