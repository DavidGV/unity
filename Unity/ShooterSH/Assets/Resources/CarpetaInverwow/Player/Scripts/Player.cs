﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Personaje
/// </summary>
[CreateAssetMenu(fileName = "Player", menuName = "Player", order = 5)]
public class Player : Personaje
{
    public GameObject Arma;

    /// <summary>
    /// tamaño del inventario
    /// </summary>
    public static int size;

    /// <summary>
    /// Inventario de objetos del player
    /// </summary>
    public Dictionary<GameObject, int> Inventario = new Dictionary<GameObject, int>();

}

