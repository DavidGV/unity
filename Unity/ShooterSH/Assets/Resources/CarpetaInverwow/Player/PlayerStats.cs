﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public Player stats;
    // Start is called before the first frame update
    void Start()
    {
        if (stats.vida > 100)
        {
            stats.vida = 100;
        }
    }

    public void daño()
    {
        this.stats.vida -= 20;
        if (stats.vida <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
