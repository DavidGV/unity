﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ScriptableObject de las armas
/// </summary>

[CreateAssetMenu(fileName = "Arma", menuName = "Arma", order = 0)]



public class Arma : ScriptableObject {


    public new string name;

    /// <summary>
    /// Descripcion del arma para el usuario
    /// </summary>
    public string descripcion;

    [Space]
    [Space]
    /// <summary>
    /// Tipo de disparo(AUTOMATICO,RAFAGAS(de 3), SEMIAUTOMATICO
    /// </summary>
    public ModoDisparo modoDisparo;



    /// <summary>
    ///  Solo tiene sentido aplicarle valor si el arma esta en <see cref="ModoDisparo.RAFAGAS"/>
    ///  Indica el tiempo entre bala y bala de una rafaga
    /// </summary>
    public float velocidadRafaga;


    /// <summary>
    ///  Solo tiene sentido aplicarle valor si el arma esta en <see cref="ModoDisparo.RAFAGAS"/>
    ///  Indica las balas que disparara una rafaga
    /// </summary>
    public int balasPorRafaga;


    [Space]
    [Space]

    /// <summary>
    /// Municion que contiene cada cargador
    /// </summary>
    public int cargador;

    /// <summary>
    /// Municion del cargador actual
    /// </summary>
    public int cargadorActual;

    [Space]
    [Space]

    /// <summary>
    /// Municion  actual
    /// </summary>
    public int municionActual;

    /// <summary>
    /// Municion maxima
    /// </summary>
    public int municionMaxima;

    [Space]
    [Space]

    /// <summary>
    /// Tiempo de la recarga una vez se ha vaciado el cargador
    /// </summary>
    public float tiempoRecarga;





    /// <summary>
    /// Tipo de unidad TEMPORAL, retroceso de la camara al disparar
    /// </summary>
    public float retroceso;
    /// <summary>
    /// Cooldown del arma (tiempo entre disparo y disparo).
    /// </summary>
    public float cadencia;

  

    [Space]
    [Space]
    /// <summary>
    /// Daño por proyectil
    /// </summary>
    public float daño;
    [Space]
    /// <summary>
    /// dispersion del arma, para armas precisas, usar 0,00x
    /// </summary>
    public float dispersion;

    /// <summary>
    /// Momentum (fuerza que hace el raycast) al objetivo
    /// </summary>
    public float momentum;

    /// <summary>
    /// Alcance efectivo del arma (metros), si impacta a mas distancia, pues penalizacion
    /// </summary>
    public float alcanceEfectivo;

    /// <summary>
    /// Numero de raycasts del arma
    /// </summary>
    public int proyectiles;

    [Space]
    [Space]

    /// <summary>
    /// Sonido de disparo del arma
    /// </summary>
    public AudioClip shootSound;

 

    /// <summary>
    /// Efecto que hara el raycast al impactar
    /// </summary>
    public GameObject efecto;






    public enum ModoDisparo
    {
        /// <summary>
        /// Dispara automaticamente con un cooldown muy bajo
        /// </summary>
        AUTOMATICO,
        /// <summary>
        /// Dispara en rafagas de 3
        /// </summary>
        RAFAGAS,    
        /// <summary>
        /// Para francotiradores, escopetas y bazocas
        /// </summary>
        SEMIAUTOMATICO
           
              
            
            
    }
    
    public class MunicionYRecargas 
    {
        public string coso;
    }

    


}

