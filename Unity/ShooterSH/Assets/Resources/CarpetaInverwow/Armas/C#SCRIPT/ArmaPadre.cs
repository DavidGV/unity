﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
/// <summary>
/// Implementa el ScriptableObject <see cref="Arma"/>
/// Clase de la cual heredaran todas las armas que se crean
/// </summary>
public abstract class ArmaPadre : MonoBehaviour
{
    [SerializeField]
    public Arma arma;
    public Camera camera;
    public AudioSource audioSource;

    /// <summary>
    /// float que indica la longitud del arma, viene de />
    /// </summary>
    public float longitudArma;

    /// <summary>
    /// Se encarga de permitir disparar o no al arma, facil vrd?
    /// </summary>
    public bool canShoot;

    public Animator animator;

  

    private void Awake()
    {
        audioSource = this.gameObject.AddComponent<AudioSource>();
        animator = this.GetComponentInParent<Animator>();
       
    }
    void Start()
    {
        canShoot = true;
        arma.cargadorActual = arma.cargador;
        
    }

    public void Update()
    {
        if (this.transform.parent.parent.GetComponent<PhotonView>().IsMine)
        {
            if (Input.GetKeyDown("r"))
            {
                reload();
            }
            if (checkDisparo())
            {
                if (arma.modoDisparo == Arma.ModoDisparo.AUTOMATICO && Input.GetMouseButton(0))
                {
                    Shoot();
                    StartCoroutine(cadenciaTiro());
                }
                else if (arma.modoDisparo == Arma.ModoDisparo.RAFAGAS && Input.GetMouseButtonDown(0))
                {


                    StartCoroutine(rafagaLapse(arma.balasPorRafaga));
                    canShoot = false;


                }
                else if (arma.modoDisparo == Arma.ModoDisparo.SEMIAUTOMATICO && Input.GetMouseButtonDown(0))
                {
                    Shoot();
                    StartCoroutine(cadenciaTiro());
                    //canShoot = true;
                }
            }
        }
        else
        {//PUTAMADRE
            this.transform.parent.GetComponent<Camera>().enabled = false;
        }
    }
   

    /// <summary>
    /// Dispara, se ejecuta una vez por cada Arma.proyectiles
    /// La parte del switch solo mira el armaActual y activa su fogonazo
    /// </summary>
    
    public void Shoot()
    {
        RaycastHit hit;

        audioSource.PlayOneShot(arma.shootSound);
        //camera.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
        switch (Apuntar.armaActual)
        {
            case 1:
                camera.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 2:
                camera.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 3:
                camera.transform.GetChild(4).transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 4:
                camera.transform.GetChild(6).transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 5:
                camera.transform.GetChild(8).transform.GetChild(0).gameObject.SetActive(true);
                break;
            default:
                break;
        }
        StartCoroutine(desactivarFogonazo());
        //TIENE QUE ESTAR EN LA POSICION DE LA ARMA + EL FORWARD DEL ARMA * POR LA LONGITUD DEL ARMA
        // GameObject newFogonazo = Instantiate(arma.fogonazo);
        //newFogonazo.transform.parent = camera.transform;
        // newFogonazo.transform.localPosition = new Vector3(-0.706f, 0.037f, 0);

        //newFogonazo.transform.localPosition = camera.transform.GetChild(0).transform.forward * longitudArma;
        //Destroy(newFogonazo, 0.2f);

        bool apuntando = animator.GetBool("Apuntar");
        bool ragdoll=true;
        for (int i = 0; i < arma.proyectiles; i++)
        {
            Vector3 newForward;
            if (!apuntando) { 
             newForward = new Vector3(
                camera.transform.forward.x + Random.Range(-arma.dispersion, arma.dispersion),
                camera.transform.forward.y + Random.Range(-arma.dispersion, arma.dispersion),
                camera.transform.forward.z + Random.Range(-arma.dispersion, arma.dispersion));
            }
            else
            {
                 newForward = new Vector3(
                camera.transform.forward.x + Random.Range(-arma.dispersion/5, arma.dispersion/5),
                camera.transform.forward.y + Random.Range(-arma.dispersion/5, arma.dispersion/5),
                camera.transform.forward.z + Random.Range(-arma.dispersion/5, arma.dispersion/5));
            }

            if (Physics.Raycast(camera.transform.position, newForward, out hit, arma.alcanceEfectivo))
            {
                Debug.DrawLine(camera.transform.position, hit.point, Color.green, 1f);
                if (hit.transform.tag == "Disparable")
                {
                    //aqui saltara el evento
                    hit.transform.gameObject.GetComponent<Disparable>().personaje.vida -= arma.daño;
                    if (hit.transform.gameObject.GetComponent<Disparable>().personaje.vida <= 0&&ragdoll)
                    {
                        hit.transform.gameObject.GetComponent<Disparable>().kill();
                        ragdoll=false;
                    }
                   
                }
               /* if (hit.transform.gameObject.GetComponent<Rigidbody>() != null) { 
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * arma.momentum, hit.point);
                }*/
                GameObject newBengala = Instantiate(arma.efecto);
                newBengala.transform.position = hit.point;
                Destroy(newBengala, 1f);

            }

        }
        arma.cargadorActual--;
        if (this.arma.modoDisparo == Arma.ModoDisparo.AUTOMATICO||this.arma.modoDisparo == Arma.ModoDisparo.SEMIAUTOMATICO)
        {
            canShoot = false;
        }

    }
  


    public bool checkDisparo()
    {
        if (arma.cargadorActual > 0 && canShoot)
        {
            return true;
        }
        else return false;
    }

    public bool reload()
    {
        if (arma.cargadorActual == arma.cargador || arma.municionActual == 0)
        {
            Debug.Log("no se puede recargar");
            return false;
        }
        else
        {
            Debug.Log("recargando");
            canShoot = false;
            StartCoroutine(recargando());
            //a) hay suficiente municion para rellenar el cargador
            if (arma.municionActual >= arma.cargador - arma.cargadorActual)
            {
                int faltan = 0;
                faltan = arma.cargador - arma.cargadorActual;

                arma.cargadorActual += faltan;
                arma.municionActual -= faltan;
            }
            else //b) no hay suficiente municion para rellenar el cargador, pero hay municion
            {
                arma.cargadorActual += arma.municionActual;
                arma.municionActual = 0;
            }


        }

        return true;
    }

    #region corrutinas
    /// <summary>
    /// Corrutina dependiente del arma.cadencia que pone el booleano a true al terminar
    /// </summary>
    /// <returns></returns>
    IEnumerator cadenciaTiro()
    {
        yield return new WaitForSeconds(1/arma.cadencia);
        canShoot = true;
    }
    /// <summary>
    /// corrutina que no permite disparar mientras recargas
    /// </summary>
    /// <returns></returns>
    IEnumerator recargando()
    {
        yield return new WaitForSeconds(arma.tiempoRecarga);
        canShoot = true;
    }
    /// <summary>
    /// <see cref="Arma.balasPorRafaga"/>
    /// <seealso cref="Arma.velocidadRafaga"/>
    /// 
    /// </summary>
    /// <param name="iterations"></param>
    /// <returns></returns>
    IEnumerator rafagaLapse(int iterations)
    {
        Debug.Log(iterations);
        Shoot();
        yield return new WaitForSeconds(arma.velocidadRafaga);
        iterations--;
        if (iterations > 0 && arma.cargadorActual > 0)
        {
            Debug.Log("hola'?");
            StartCoroutine(rafagaLapse(iterations));
        }
        else StartCoroutine(cadenciaTiro());

    }
    /// <summary>
    /// Desactiva el fogonazo del arma
    /// </summary>
    /// <returns></returns>
    private IEnumerator desactivarFogonazo()
    {
        yield return new WaitForSeconds(0.2f);
        camera.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
        camera.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
        camera.transform.GetChild(4).transform.GetChild(0).gameObject.SetActive(false);
        camera.transform.GetChild(6).transform.GetChild(0).gameObject.SetActive(false);
        camera.transform.GetChild(8).transform.GetChild(0).gameObject.SetActive(false);
        /*switch (Apuntar.armaActual)
        {
            case 1:
                camera.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
                break;
            case 2:
                camera.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
                break;
            case 3:
                camera.transform.GetChild(4).transform.GetChild(0).gameObject.SetActive(false);
                break;
            case 4:
                camera.transform.GetChild(6).transform.GetChild(0).gameObject.SetActive(false);
                break;
            case 5:
                camera.transform.GetChild(8).transform.GetChild(0).gameObject.SetActive(false);
                break;
            default:
                break;
        }*/
        
    }




    #endregion

}

