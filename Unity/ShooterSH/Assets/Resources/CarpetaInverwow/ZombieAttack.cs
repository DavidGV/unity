﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieAttack : MonoBehaviour
{
    bool flag=true;
    public Zombie zombie;
    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("dentro de colision");
        if (collision.gameObject.tag == "Player"&&flag)
        {
            Debug.Log("dentro del if de zombieatack");
            collision.gameObject.GetComponent<PlayerStats>().daño();
            flag = false;
            StartCoroutine(cadencia());
        }
        
    }
    

    IEnumerator cadencia()
    {
        yield return new WaitForSeconds(zombie.velocidadAtaque);
        flag = true;
    }

    
    
}
