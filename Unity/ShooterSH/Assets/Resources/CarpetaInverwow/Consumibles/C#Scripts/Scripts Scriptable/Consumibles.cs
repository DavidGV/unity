﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable Object  que engloba las propiedades comunes de los consumibles
/// </summary>


public  class Consumibles : ScriptableObject
{
    public new string name;
    /// <summary>
    /// Descripcion del consumible
    /// </summary>
    public string descripcion;
    /// <summary>
    /// Unidades maximas que puedes portar
    /// </summary>
    public int unidadesMaximas;
    /// <summary>
    /// Unidades que llevas
    /// </summary>
    public int unidadesActuales;



}
