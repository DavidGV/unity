﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable Object orientado a objetos de sanacion
/// </summary>
/// 


[CreateAssetMenu(fileName = "ConsumiblesDaño", menuName = "ConsumiblesDáño", order = 2)]
public class ConsumiblesDaño : Consumibles
{
    /// <summary>
    /// Daño que te hace el objeto al impactar en un personaje
    /// </summary>
    public float dañoAlImpactar;
    /// <summary>
    /// Tiempo que tardas en poder volver a usar el objeto
    /// </summary>
    public float cooldown;
    /// <summary>
    /// Tiempo que tarda en explotarte en la mano lo que tengas
    /// </summary>
    public float demora;
    /// <summary>
    /// Radio de alcance del arma
    /// </summary>
    public float radio;
    

}
