﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable Object orientado a objetos de sanacion
/// </summary>
/// 


[CreateAssetMenu(fileName = "ConsumiblesMedicos", menuName = "ConsumibleMedico", order = 1)]
public class ConsumiblesMedicos : Consumibles
{
    /// <summary>
    /// Vida que cura
    /// </summary>
    public int hpAcurar;
    /// <summary>
    /// Tiempo que tardas en poder volver a curarte
    /// </summary>
    public float cooldown;
    /// <summary>
    /// Tiempo que tardas en curarte
    /// </summary>
    public float tiempoAcurarte;

}
