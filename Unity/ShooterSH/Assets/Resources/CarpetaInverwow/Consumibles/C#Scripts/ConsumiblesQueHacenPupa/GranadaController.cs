﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadaController : Arrojadizos
{
    public ConsumiblesDaño granada;
    public GameObject granadaObject;
    public bool haInstanciado = false;
    private GameObject newGranada;
    public Camera playerCamara;

    private void FixedUpdate()
    {

        if (Input.GetKey("g") && granada.unidadesActuales > 0)
        {
            if (haInstanciado == false)
            {
                granada.unidadesActuales--;
                newGranada = Instantiate(granadaObject);
                newGranada.GetComponent<Granada>().granadaController = this;
               // newGranada.transform.position = this.transform.position;
                newGranada.GetComponent<Rigidbody>().isKinematic = true;
                StartCoroutine(newGranada.GetComponent<Granada>().comienzoDemora(granada));
                haInstanciado = true;
            }




        }
        if (Input.GetKeyUp("g") && haInstanciado == true)
        {
            Lanzar();
            haInstanciado = false;
        }


    }

    IEnumerator puedeLanzar()
    {
        yield return new WaitForSeconds(0.5f);
    }
    public override void Lanzar()
    {

        newGranada.GetComponent<Rigidbody>().isKinematic = false;
        Vector3 newForward = playerCamara.transform.forward;
        newForward.y += 15;
        newForward.x += 15;
        newGranada.GetComponent<Rigidbody>().AddForce(newForward * 20);

    }


}
