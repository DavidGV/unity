﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granada : MonoBehaviour
{
    private ConsumiblesDaño granadaStats;
    public GranadaController granadaController;

    public void FixedUpdate()
    {
        if (granadaController.haInstanciado == true)
        {
            this.transform.position = granadaController.transform.position;
            this.transform.localPosition += new Vector3(0, 1, 0);
        }
    }


    public IEnumerator comienzoDemora(ConsumiblesDaño granadaStats)
    {
        this.granadaStats = granadaStats;
        yield return new WaitForSeconds(granadaStats.demora);
        Bum();
    }

    public void Bum()
    {
        //el add explosion force se añade a los objetos que reciben el guantazo

        this.GetComponent<SphereCollider>().radius = granadaStats.radio;
        this.GetComponent<SphereCollider>().enabled = true;
        StartCoroutine(destruccion());
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.GetComponent<Rigidbody>().AddExplosionForce(1000f, this.transform.position, granadaStats.radio);
    }

    IEnumerator destruccion()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}
