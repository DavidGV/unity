﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject[] spanwnsControllers;
    public GameEventListener endedSpawn;
    public int sinTerminar;
    public int zombiesMax = 0;
    public int zombiesActuales = 0;
    public int zombiesTotalesInstanciados = 0;
    bool flag = true;
    private void Awake()
    {

        sinTerminar = spanwnsControllers.Length;

    }
    private void Start()
    {
        foreach (GameObject spawn in spanwnsControllers)
        {
            zombiesMax += spawn.GetComponent<SpawnController>().spawnData.zombiesXRound;
        }

    }
    private void FixedUpdate()
    {

        foreach (GameObject spawn in spanwnsControllers)
        {

            if (zombiesTotalesInstanciados == zombiesMax)
            {
                Debug.Log("entra aqui pa algo?");
                flag = false;
                StartCoroutine(cambiarcoso());
            }
            else
            {
                if (flag)
                {
                    //no hace falta comprobacion, la hace en la propia funcion!
                    Debug.Log("1 if ");
                    GameObject zombie = spawn.GetComponent<SpawnController>().spawnZombie();
                    if (zombie != null)
                    {
                        Debug.Log("2 if ");
                        // zombie.GetComponent<Disparable>().eliminacion.SubscribeListener(endedSpawn); esto mal
                        zombiesActuales++;
                        zombiesTotalesInstanciados++;
                    }
                }
            }
        }
    }

    public void zombieAmochao()
    {
        Debug.Log("evento");
        zombiesActuales--;
        if (zombiesActuales == 0)
        {
            //reinicio?
            zombiesTotalesInstanciados = 0;
        }
    }
    IEnumerator cambiarcoso()
    {
        flag = false;
        yield return new WaitForSeconds(5f);
        flag = true;
    }


}
