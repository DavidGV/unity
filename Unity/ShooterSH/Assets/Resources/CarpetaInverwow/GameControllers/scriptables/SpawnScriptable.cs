﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scriptable Object orientado a objetos de sanacion
/// </summary>
/// 


[CreateAssetMenu(fileName = "Spawn", menuName = "ZombieSpawn", order = 7)]
public class SpawnScriptable : ScriptableObject
{
    public int zombiesXRound;
    public float velocitySpawn;


}