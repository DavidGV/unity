﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject gameController;
    public GameObject zombie;
    private int zombiesInstanciados;
    private bool canInstantiate = true;

    public SpawnScriptable spawnData;
    private int numZombies;

    
    public GameObject spawnZombie()
    {
        if (canInstantiate)
        {
            GameObject newZombie = Instantiate(zombie);
            newZombie.transform.position = this.transform.position;
            canInstantiate = false;
            StartCoroutine(nextZombie());
            return newZombie;
        }
        return null;
    }

    IEnumerator nextZombie()
    {
        yield return new WaitForSeconds(spawnData.velocitySpawn);
        canInstantiate = true;
    }

    //funcion Response() de listenerZombies
    /*public void zombieMuerto()
    {
        
        
    }*/

    


}
