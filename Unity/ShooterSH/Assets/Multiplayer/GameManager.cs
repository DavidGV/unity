﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

namespace Photon.Pun.Demo.PunBasics
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public GameObject player1SpawnPosition;
        public GameObject player2SpawnPosition;

        
        private GameObject zombie;
        private GameObject zombie2;
        private GameObject zombie3;
        public GameObject zombie1SpawnPosition;
        public GameObject zombie2SpawnPosition;
        public GameObject zombie3SpawnPosition;

        private GameObject player1;
        private GameObject player2;
        // Start is called before the first frame update
        void Start()
        {
            if (!PhotonNetwork.IsConnected) //si no hi ha connexio et torna al lobby
            {
                SceneManager.LoadScene("Lobby");
                return;
            }

            if (PlayerManager.LocalPlayerInstance == null)
            {
                if (PhotonNetwork.IsMasterClient) //aquests passos només els fa el host
                {
                    Debug.Log("Instantiating Player 1");
                    // MOLT IMPORTANT: QUALSEVOL PREFAB QUE HAGIS D'INSTANCIAR PER TOTHOM HA D'ESTAR EN LA CARPETA RESOURCES A ARREL
                    player1 = PhotonNetwork.Instantiate("FPSController", player1SpawnPosition.transform.position, player1SpawnPosition.transform.rotation, 0);
                    zombie = PhotonNetwork.Instantiate("ZombieRun", zombie1SpawnPosition.transform.position, zombie1SpawnPosition.transform.rotation, 0);
                    zombie2 = PhotonNetwork.Instantiate("ZombieRun", zombie2SpawnPosition.transform.position, zombie2SpawnPosition.transform.rotation, 0);
                    zombie3 = PhotonNetwork.Instantiate("ZombieRun", zombie3SpawnPosition.transform.position, zombie3SpawnPosition.transform.rotation, 0);
                }
                else //si no ets el hsot
                {

                    player2 = PhotonNetwork.Instantiate("FPSController", player2SpawnPosition.transform.position, player2SpawnPosition.transform.rotation, 0);
                   // cubot = PhotonNetwork.Instantiate("PRUEBA_COSA", cubo.transform.position, cubo.transform.rotation, 0);
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
