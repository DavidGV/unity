﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class LobbyController : MonoBehaviourPunCallbacks
{
    public GameObject player;
    public GameObject plataforma;

    public Button btnConnect;
    public Button btnJoin;
    public Button btnStart;
    public Text msg;
    public Text JugadoresConectados;

    private int maxPlayers = 4;
    public int playerCount;

    void Awake()
    {
        //defineix que tots els clients haurien de tenir la mateixa escena que el master client (el host)
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Start()
    {
        //esborrem els playerprefs. Photon els fa servir aixi que millor esborrarho tot
        PlayerPrefs.DeleteAll();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        player.transform.Rotate(0, 1, 0);
        plataforma.transform.Rotate(0, 1, 0);

        if (PhotonNetwork.CurrentRoom != null)
        {
            playerCount = PhotonNetwork.CurrentRoom.PlayerCount;
            JugadoresConectados.text = "Players: " + playerCount + "/" + maxPlayers;
        }
        
    }

    public void Connect() 
    {
        if (!PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.ConnectUsingSettings())
            {
                msg.text = "Connected to Server";
                msg.GetComponent<Text>().color = Color.green;
            }
            else
            {
                msg.text = "Failing Connecting to Server";
                msg.GetComponent<Text>().color = Color.red;
            }
        }

    }
    public override void OnConnectedToMaster()
    {
        btnConnect.interactable = false;
        btnJoin.interactable = true;
    }
    //public void JoinRandom()
    //{
    //    if (!PhotonNetwork.JoinRandomRoom())
    //    {
    //        msg.text = "Fail Joining Room";
    //        msg.GetComponent<Text>().color = Color.red;
    //    }
    //}

    public void JoinRoom()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.LocalPlayer.NickName = "Player"; //definira el nom amb el que et poden veure
            Debug.Log("PhotonNetwork.IsConnected! | Trying to Create/Join Room");
            RoomOptions roomOptions = new RoomOptions(); //crees una Room. Forma part de Photon. 
            TypedLobby typedLobby = new TypedLobby("Room", LobbyType.Default); //Crees un lobby
            PhotonNetwork.JoinOrCreateRoom("Room", roomOptions, typedLobby); //si roomName no existeix la crea. si existeix s'uneix.
        }
        else
        {
            msg.text = "Fail Joining Room";
            msg.GetComponent<Text>().color = Color.red;
        }
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        if (PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions()))
        {
            msg.text = "Created Room";
            msg.GetComponent<Text>().color = Color.green;
        }
        else
        {
            msg.text = "Failing Creating Room";
            msg.GetComponent<Text>().color = Color.red;
        }
    }
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            msg.text = "Joinned Room";
            msg.GetComponent<Text>().color = Color.green;
            btnJoin.interactable = false;
            btnStart.interactable = true;
            msg.text = "Your are Lobby Leader";
        }
        else
        {
            btnStart.transform.GetChild(0).GetComponent<Text>().text = "Esperando a que el host inicie partida";
            btnJoin.interactable = false;
            msg.text = "Connected to Lobby";
        }
        
    }
    public void LoadArena()
    {
        // si hi ha mes d'un jugador fas un LoadLevel que canvia l'escena. Com que les escenes estan sincronitzades canvia per tothom
        if (PhotonNetwork.CurrentRoom.PlayerCount >= 1)
        {
            PhotonNetwork.LoadLevel("Map_v1");
        }
        else
        {
            msg.text = "Esperando a que se unan más jugadores";
        }
    }
}
